<?php include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php");
include "./admin/db/db.php" ?>
<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from html.physcode.com/travel/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:52:27 GMT -->
<head>
    <title>Home</title>
    <?php include "./layout/head.php" ?>
</head>
<body class="transparent_home_page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="home-content" role="main">
            <div class="wrapper-bg-video">
                <video poster="<?php globalUrl($cdUpRefArray) ?>images/banner/<?php echo $video['images'] ?>"
                       playsinline autoplay muted loop>
                    <source src="<?php globalUrl($cdUpRefArray) ?>images/banner/<?php echo $video['video'] ?>"
                            type="video/mp4">
                </video>
                <div class="content-slider">
                    <?php echo $video['describe'] ?>
                    <p><a href="tours.php" class="btn btn-slider">Xem Tour </a></p>
                </div>
            </div>


            <div class="container two-column-respon mg-top-6x mg-bt-6x">
                <div class="row">
                    <div class="col-sm-12 mg-btn-6x">
                        <div class="shortcode_title title-center title-decoration-bottom-center">
                            <h3 class="title_primary">Tại sao chọn chúng tôi?</h3><span class="line_after_title"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="wpb_column col-sm-3">
                        <div class="widget-icon-box widget-icon-box-base iconbox-center">
                            <div class="boxes-icon circle"
                                 style="font-size:30px;width:80px; height:80px;line-height:80px">
                                <span class="inner-icon"><i
                                            class="vc_icon_element-icon flaticon-transport-6"></i></span>
                            </div>
                            <div class="content-inner">
                                <div class="sc-heading article_heading">
                                    <h4 class="heading__primary">Điểm đến đa dạng</h4></div>

                            </div>
                        </div>
                    </div>
                    <div class="wpb_column col-sm-3">
                        <div class="widget-icon-box widget-icon-box-base iconbox-center">
                            <div class="boxes-icon " style="font-size:30px;width:80px; height:80px;line-height:80px">
                                <span class="inner-icon"><i class="vc_icon_element-icon flaticon-sand"></i></span>
                            </div>
                            <div class="content-inner">
                                <div class="sc-heading article_heading">
                                    <h4 class="heading__primary">Giá trị của đồng tiền</h4></div>
                                <!--                                <div class="desc-icon-box">-->
                                <!--                                    <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</div>-->
                                <!--                                </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column col-sm-3">
                        <div class="widget-icon-box widget-icon-box-base iconbox-center">
                            <div class="boxes-icon " style="font-size:30px;width:80px; height:80px;line-height:80px">
                                <span class="inner-icon"><i class="vc_icon_element-icon flaticon-travel-2"></i></span>
                            </div>
                            <div class="content-inner">
                                <div class="sc-heading article_heading">
                                    <h4 class="heading__primary">Địa điểm đẹp</h4></div>

                            </div>
                        </div>
                    </div>
                    <div class="wpb_column col-sm-3">
                        <div class="widget-icon-box widget-icon-box-base iconbox-center">
                            <div class="boxes-icon circle"
                                 style="font-size:30px;width:80px; height:80px;line-height:80px">
                                <span class="inner-icon"><i class="vc_icon_element-icon flaticon-travelling"></i></span>
                            </div>
                            <div class="content-inner">
                                <div class="sc-heading article_heading">
                                    <h4 class="heading__primary">Du lịch đam mê</h4></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="padding-top-6x padding-bottom-6x section-background"
                 style="background-image:url(images/home/bg-popular.jpg)">
                <div class="container">
                    <div class="shortcode_title text-white title-center title-decoration-bottom-center">
                        <div class="title_subtitle">
                            Hãy xem của chúng tôi
                        </div>
                        <h3 class="title_primary">Tour du lịch nổi bật</h3>
                        <span class="line_after_title" style="color:#ffffff"></span>
                    </div>
                    <div class="row wrapper-tours-slider">
                        <div class="tours-type-slider list_content" data-dots="true" data-nav="true"
                             data-responsive='{"0":{"items":1}, "480":{"items":2}, "768":{"items":2}, "992":{"items":3}, "1200":{"items":4}}'>
                            <?php foreach ($tour_special as $rows) { ?>
                                <div class="item-tour">
                                    <div class="item_border">
                                        <div class="item_content">
                                            <div class="post_images">
                                                <a href="single-tour.php?id=<?php echo $rows['id'] ?>"
                                                   class="travel_tour-LoopProduct-link">
											<span class="price"><del>
												<span class="travel_tour-Price-amount amount">$<?php echo $rows['tour_cost'] ?></span></del>
												<ins><span class="travel_tour-Price-amount amount">$<?php echo $rows['tour_sale'] ?></span></ins>
											</span>
                                                    <?php if (isset($rows['tour_sale'])) { ?>
                                                        <span class="onsale">Sale!</span>
                                                    <?php } else {
                                                        echo "";
                                                    } ?>

                                                    <img src="images/tour/detail/<?php echo $rows['images'] ?>" alt=""
                                                         title="">
                                                </a>

                                            </div>
                                            <div class="wrapper_content">
                                                <div class="post_title"><h4>
                                                        <a href="single-tour.php?id=<?php echo $rows['id'] ?>"
                                                           rel="bookmark"><?php echo $rows['tour_name'] ?></a>
                                                    </h4></div>
                                                <span class="post_date"><?php echo $rows['duration'] ?></span>
                                                <p><?php echo $rows['short_desc'] ?></p>
                                            </div>
                                        </div>
                                        <div class="read_more">
                                            <div class="item_rating">
                                                <?php if ($rows['voting'] == 0) { ?>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>

                                                <?php } else if ($rows['voting'] == 1) { ?>
                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <?php
                                                } else if ($rows['voting'] == 2) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                <?php } else if ($rows['voting'] == 3) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                <?php } else if ($rows['voting'] == 4) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                <?php } else if ($rows['voting'] == 5) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>

                                                <?php } ?>

                                            </div>
                                            <a href="single-tour.php?id=<?php echo $rows['id'] ?>"
                                               class="read_more_button">VIEW MORE
                                                <i class="fa fa-long-arrow-right"></i></a>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>


                        </div>
                    </div>
                </div>
            </div>
            <div class="section-white padding-top-6x padding-bottom-6x tours-type">
                <div class="container">
                    <div class="shortcode_title title-center title-decoration-bottom-center">
                        <div class="title_subtitle">Tìm Tour theo</div>
                        <h3 class="title_primary">Thể Loại</h3><span class="line_after_title"></span>
                    </div>
                    <div class="wrapper-tours-slider wrapper-tours-type-slider">
                        <div class="tours-type-slider" data-dots="true" data-nav="true"
                             data-responsive='{"0":{"items":1}, "480":{"items":2}, "768":{"items":3}, "992":{"items":4}, "1200":{"items":5}}'>
                            <?php foreach ($category as $category_rows) { ?>
                                <div class="tours_type_item">
                                    <a href="tours.php?id=<?php echo $category_rows['id'] ?>"
                                       class="tours-type__item__image">
                                        <img style="height: 200px !important;"
                                             src="images/tour/category/<?php echo $category_rows['images'] ?>">
                                    </a>
                                    <div class="content-item">
                                        <div class="item__title"><?php echo $category_rows['category'] ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="padding-top-6x padding-bottom-6x bg__shadow section-background"
                 style="background-image:url(images/home/bg-pallarax.jpg)">
                <div class="container">
                    <div class="shortcode_title text-white title-center title-decoration-bottom-center">
                        <div class="title_subtitle">Một số thống kê về Travel</div>

                        <span class="line_after_title" style="color:#ffffff"></span>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="stats_counter text-center text-white">
                                <div class="wrapper-icon">
                                    <i class="flaticon-airplane"></i>
                                </div>
                                <?php $user_sql = "SELECT COUNT(id) as user_count FROM `users`";
                                $users = $conn->query($user_sql)->fetch() ?>
                                <div class="stats_counter_number"><?php echo $users['user_count'] ?></div>
                                <div class="stats_counter_title">Customers</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="stats_counter text-center text-white">
                                <div class="wrapper-icon">
                                    <i class="flaticon-island"></i>
                                </div>
                                <?php $location_sql = "SELECT COUNT(location) as location FROM `tour_list` ";
                                $location = $conn->query($location_sql)->fetch() ?>
                                <div class="stats_counter_number"><?php echo $location['location'] ?></div>
                                <div class="stats_counter_title">Destinations</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="stats_counter text-center text-white">
                                <div class="wrapper-icon">
                                    <i class="flaticon-globe"></i>
                                </div>
                                <?php $location_sql = "SELECT COUNT(id) as tour FROM `tour_list` ";
                                $location = $conn->query($location_sql)->fetch() ?>
                                <div class="stats_counter_number"><?php echo $location['tour'] ?></div>
                                <div class="stats_counter_title">Tours</div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center padding-top-6x">
                            <a href="tours.php" class="icon-btn" title="" target="_blank">
                                <i class="flaticon-airplane-4"></i>Xem tour
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<script type='text/javascript' src='assets/js/jquery.min.js'></script>
<script type='text/javascript' src='assets/js/bootstrap.min.js'></script>
<script type='text/javascript' src='assets/js/vendors.js'></script>
<script type='text/javascript' src='assets/js/owl.carousel.min.js'></script>
<script type="text/javascript" src="assets/js/jquery.mb-comingsoon.min.js"></script>
<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
<script type='text/javascript' src='assets/js/theme.js'></script>
</body>

<!-- Mirrored from html.physcode.com/travel/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:54:59 GMT -->
</html>