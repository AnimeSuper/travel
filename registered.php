<?php

include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php"); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <div class="form_popup from_login">
                    <div class="inner-form">
                        <div class="closeicon"></div>
                        <h3>Đăng ký</h3>
                        <form action="" method="post">
                            <p>
                                <label for="first_name">Họ</label>
                                <input required style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="text" name="first_name" value=""
                                       size="20">
                            </p>
                            <p>
                                <label for="last_name">Tên</label>
                                <input required style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="text" name="last_name" value=""
                                       size="20">
                            </p>

                            <p class="login-username">
                                <label for="user_login">Tài Khoản</label>
                                <input required style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="text" name="log" value=""
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Mật Khẩu</label>
                                <input required style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="password" name="pwd" value=""
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label required for="user_pass">Nhập lại mật khẩu</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="password" name="re_pwd" value=""
                                       size="20">
                            </p>
                            <p>
                                <label for="email">Email</label>
                                <input required style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="text" name="email" value=""
                                       size="20">
                            </p>
                            <p>
                                <label for="last_name">Số điện thoại</label>
                                <input required style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="text" name="phone_number" value=""
                                       size="20">
                            </p>
                            <p class="login-submit">
                                <input style="width: 100%;
    padding: 12px 15px;
    text-align: center;
    background: #2a2a2a;
    color: #fff;
    border: none;
    text-transform: uppercase;
    max-width: 190px;" type="submit" name="btn_login"
                                       class="button button-primary" value="Đăng ký">

                            </p>
                        </form>

                    </div>
                    <?php
                    if (isset($_POST['btn_login'])) {
                        $first_name = $_POST['first_name'];
                        $last_name = $_POST['last_name'];
                        $username = $_POST['log'];
                        if ($_POST['re_pwd'] == $_POST['pwd']) {
                            $password = $_POST['pwd'];
                            $password_md5 = md5($password);
                        } else {
                            echo "Mật khẩu nhập lại không đúng";
                            exit;
                        }
                        $email = $_POST['email'];
                        $phone_number = $_POST['phone_number'];

                        $user_sql = "SELECT * FROM users where username='$username'  ";

                        $users = $conn->query($user_sql)->fetch();


                        if ($username == $users['username']) {
                            echo "Tài khoản đã tồn tại";
                            exit;

                        }
                        $user1_sql = "INSERT INTO `users`(`id`, `first_name`, `last_name`, `username`, `password`, `email`, `phone_number`, `role`, `status`) 
VALUES (null ,'$first_name','$last_name','$username','$password_md5','$email','$phone_number',1,1)";
                        $result = $conn->exec($user1_sql);
                        if ($result == 1) {
                            $_SESSION['user'] = $username;
                            echo "Đăng ký thành công" . "| Đăng nhập tại đây !<a href='login.php'>Login</a>";
                        } else {
                            echo "Tài khoản chưa được đăng ký";
                        }


                    }
                    ?>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>