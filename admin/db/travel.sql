-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 04:39 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `album_list`
--

CREATE TABLE IF NOT EXISTS `album_list` (
  `id` int(11) NOT NULL,
  `id_tour` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `images` text NOT NULL,
  `created_date` date NOT NULL,
  `update_at` date NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `album_list`
--

INSERT INTO `album_list` (`id`, `id_tour`, `name`, `images`, `created_date`, `update_at`, `status`) VALUES
(8, 3, 'hamlon1', '2-nui-ham-lon.jpg', '2019-12-06', '2019-12-06', 1),
(9, 3, 'hamlon2', '1-nui-ham-lon.jpg', '2019-12-06', '2019-12-06', 1),
(10, 3, 'hamlon3', '3-nui-ham-lon.jpg', '2019-12-06', '2019-12-06', 1),
(11, 4, 'nuitram1', 'nui-tram-vntrip1-e1533285295428.jpg', '2019-12-06', '2019-12-06', 1),
(12, 4, 'nuitram2', '5-nui-tram-768x577.jpg', '2019-12-06', '2019-12-06', 1),
(13, 4, 'nuitram3', '5-nui-tram-768x577.jpg', '2019-12-06', '2019-12-06', 1),
(14, 2, 'anh 1', 'ho_guom.jpg', '2019-12-07', '2019-12-07', 1),
(15, 2, 'anh 2', '5-nui-tram-768x577.jpg', '2019-12-07', '2019-12-07', 1),
(16, 2, 'anh 3', '5-nui-tram-768x577.jpg', '2019-12-07', '2019-12-07', 1),
(17, 2, 'anh 4', '3-nui-ham-lon.jpg', '2019-12-07', '2019-12-07', 1),
(18, 3, 'anh 4', 'tour-5.jpg', '2019-12-07', '2019-12-07', 1),
(19, 4, 'anh 4', 'tour-2.jpg', '2019-12-07', '2019-12-07', 1),
(20, 5, 'anh 1', 'vuon-quoc-gia-ba-vi-vntrip1-e1533285584906.jpg', '2019-12-07', '2019-12-07', 1),
(21, 5, 'anh 2', '8-vuon-ba-vi-768x512.jpg', '2019-12-07', '2019-12-07', 1),
(22, 5, 'anh 3', 'nui-tram-vntrip1-e1533285295428.jpg', '2019-12-07', '2019-12-07', 1),
(23, 5, 'anh 4', 'quang_truong_badinh.jpg', '2019-12-07', '2019-12-07', 1),
(24, 6, 'phoco', 'phoco.jpg', '2019-12-09', '2019-12-09', 1),
(25, 6, 'phoco2', 'phoco2.jpg', '2019-12-09', '2019-12-09', 1),
(26, 6, 'phoco4', 'phoco4.jpg', '2019-12-09', '2019-12-09', 1),
(27, 6, 'pho co 3', 'phoco3.jpg', '2019-12-09', '2019-12-09', 1),
(28, 6, 'pho co 6', 'phoco1.jpg', '2019-12-09', '2019-12-09', 1),
(29, 1, 'badinh1', 'badinh1.jpg', '2019-12-10', '2019-12-10', 1),
(30, 1, 'badinh2', 'badinh2.jpg', '2019-12-10', '2019-12-10', 1),
(31, 1, 'badinh3', 'badinh3.jpg', '2019-12-10', '2019-12-10', 1),
(32, 1, 'badinh4', 'badinh4.jpg', '2019-12-10', '2019-12-10', 1),
(33, 1, 'badinh5', 'badinh3.jpg', '2019-12-10', '2019-12-10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `images` text NOT NULL,
  `describe` text NOT NULL,
  `created_date` date NOT NULL,
  `update_at` date NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`, `images`, `describe`, `created_date`, `update_at`, `status`) VALUES
(1, 'Tham Quan', 'dulichthamquan.jpg', '<p>Tham Quan</p>\r\n', '2019-12-03', '2019-12-04', 1),
(2, 'Ẩm thực', 'dulichamthuc.jpg', '<p>Du lịch ẩm thưc h&agrave; nội&nbsp;</p>\r\n', '2019-12-03', '2019-12-04', 1),
(3, 'Danh Lam Thắng cảnh', 'dulichxanh.jpg', '<p>Danh Lam Thắng cảnh</p>\r\n', '2019-12-03', '2019-12-10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cmt`
--

CREATE TABLE IF NOT EXISTS `cmt` (
  `id` int(11) NOT NULL,
  `id_tour` int(11) NOT NULL,
  `comment` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `time_post` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cmt`
--

INSERT INTO `cmt` (`id`, `id_tour`, `comment`, `id_user`, `time_post`, `status`) VALUES
(3, 2, 'tot', 4, '2019-12-04', 1),
(24, 2, 'good', 15, '2019-12-10', 1),
(25, 6, 'tot', 15, '2019-12-10', 1),
(26, 6, 'hay', 15, '2019-12-10', 1),
(27, 1, 'goood', 15, '2019-12-10', 1),
(28, 5, 'tot', 11, '2019-12-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE IF NOT EXISTS `order_list` (
  `id` int(11) NOT NULL,
  `id_tour` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `date_order` date NOT NULL,
  `people` int(11) NOT NULL,
  `id_instructor` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `id_tour`, `id_users`, `first_name`, `last_name`, `email`, `phone_number`, `date_order`, `people`, `id_instructor`, `status`) VALUES
(10, 2, 2, 'user1', '1', 'user1@gmail.com', '123456789', '0000-00-00', 1, 3, 0),
(13, 2, 2, 'chido', 'nguyen', 'animesuper7119@gmail.com', '0388240938', '0000-00-00', 1, 3, 0),
(17, 2, 11, 'tuan', 'nguyen', 'tuan@gmail.com', '123456789', '2019-12-08', 3, 3, 3),
(20, 3, 11, 'tuan1', 'nguyen', 'tuannqph08378@fpt.edu.vn', '12345679', '2019-12-10', 1, 3, 2),
(22, 1, 11, 'Tuấn', 'Nguyễn', 'animesuper7119@gmail.com', '0388240938', '2019-12-11', 3, 2, 1),
(23, 2, 15, 'N', 'T', 'animesuper7119@gmail.com', '0388240938', '2019-12-11', 3, 3, 0),
(24, 2, 15, 'N', 'T', 'animesuper7119@gmail.com', '0388240938', '2019-12-11', 3, 3, 0),
(25, 2, 11, 'Tuấn', 'Nguyễn', 'animesuper7119@gmail.com', '0388240938', '2019-12-12', 3, 3, 3),
(26, 5, 11, 'Tuấn', 'Nguyễn', 'animesuper7119@gmail.com', '0388240938', '2019-12-12', 5, 6, 2),
(28, 1, 11, 'Tuấn', 'Nguyễn', 'animesuper7119@gmail.com', '0388240938', '2019-12-12', 4, 2, 3),
(29, 1, 11, 'Tuấn123', 'Nguyễn', 'animesuper7119@gmail.com', '0388240938', '2019-12-16', 2, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `website` text NOT NULL,
  `phone_number` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `map` text NOT NULL,
  `work_time` text NOT NULL,
  `diverse_destinations` text NOT NULL,
  `value_of_money` text NOT NULL,
  `nice_place` text NOT NULL,
  `travel_enthusiasts` text NOT NULL,
  `logo` text NOT NULL,
  `terms_of_service` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `website`, `phone_number`, `email`, `address`, `map`, `work_time`, `diverse_destinations`, `value_of_money`, `nice_place`, `travel_enthusiasts`, `logo`, `terms_of_service`) VALUES
(1, 'travel', '038824093899', 'animesuper7119@gmail.com', '2019 Hà Nội , Việt Nam ', ' <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.827504537816!2d105.80170781488313!3d21.039586892784595!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab3e6416efc7%3A0x808741175914b86b!2zMTUgxJDDtG5nIFF1YW4sIFF1YW4gSG9hLCBD4bqndSBHaeG6pXksIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1573801424865!5m2!1svi!2s" width="1000"                                 height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>', 'Mon - Sat 8.00 - 18.00. Sunday CLOSED', 'Điểm đến đa dạng', 'Giá trị đồng tiền ', 'Địa điểm đẹp', 'Du lịch đam mê', 'logo_sticky.png', '<h1><strong>ĐIỀU KHOẢN ĐẶT TOUR</strong></h1>\r\n\r\n<p>A) TK C&ocirc;ng ty<br />\r\nT&ecirc;n t&agrave;i khoản: C&Ocirc;NG TY CỔ XXX</p>\r\n\r\n<p>Ng&acirc;n h&agrave;ng: BIDV,XXX,XXX</p>\r\n\r\n<p>Số T&agrave;i khoản: 1251XXXXXXXXXX</p>\r\n\r\n<p>B) TK c&aacute; nh&acirc;n Gi&aacute;m Đốc:<br />\r\nT&ecirc;n t&agrave;i khoản: Nguyễn Trung T<br />\r\nNg&acirc;n h&agrave;ng: Vietcombank, XXXXXXXXXX,XXXX<br />\r\nSố tk: 1251XXXXXXXXXX</p>\r\n\r\n<p>Khi thực hiện việc chuyển khoản, qu&yacute; kh&aacute;ch vui l&ograve;ng ghi r&otilde; họ t&ecirc;n, địa chỉ, điện thoại v&agrave; nội dung chuyến du lịch cụ thể đ&atilde; được qu&yacute; kh&aacute;ch chọn đăng k&yacute;.<br />\r\nSau khi thực hiện việc chuyển khoản, qu&yacute; kh&aacute;ch vui l&ograve;ng li&ecirc;n hệ trực tiếp người điều h&agrave;nh tour m&agrave; qu&yacute; kh&aacute;ch đang l&agrave;m việc hoặc trực tiếp gi&aacute;m đốc c&ocirc;ng ty l&agrave; &Ocirc;ng Nguyễn Trung T&nbsp;để kiểm tra lại th&ocirc;ng tin.</p>\r\n\r\n<p>3. Thanh to&aacute;n thẻ quốc tế VISA/ MASTER CARD/ JCB/ AMEX/ AMERICAN EXPRES.. th&ocirc;ng qua m&aacute;y c&agrave; thẻ tại C&ocirc;ng ty Du lịch Trực Tuyến.</p>\r\n\r\n<p>I) ĐIỀU KHOẢN CHUNG</p>\r\n\r\n<p>Gi&aacute; tất cả c&aacute;c dịch vụ được t&iacute;nh theo đơn vị vnđ. Gi&aacute; dịch vụ trong tour được liệt k&ecirc; một c&aacute;ch r&otilde; r&agrave;ng trong phần &ldquo;Bao gồm&rdquo; trong c&aacute;c tour du lịch, dịch vụ xe, kh&aacute;ch sạn,&hellip; HNTRAVEL kh&ocirc;ng c&oacute; nghĩa vụ thanh to&aacute;n bất cứ chi ph&iacute; n&agrave;o kh&ocirc;ng nằm trong phần &ldquo;Bao gồm&rdquo;.<br />\r\nTrường hợp kh&aacute;ch thanh to&aacute;n bằng USD sẽ được quy đổi ra VNĐ theo tỉ gi&aacute; của ng&acirc;n h&agrave;ng Ng&acirc;n h&agrave;ng thương mại cổ phần Ngoại thương Việt Nam &nbsp;tại thời điểm thanh to&aacute;n.<br />\r\nII) THANH TO&Aacute;N</p>\r\n\r\n<p>Khi thanh to&aacute;n&nbsp;qu&yacute; kh&aacute;ch vui l&ograve;ng cung cấp đầy đủ th&ocirc;ng tin v&agrave; đ&oacute;ng một khoản tiền cọc để giữ chỗ. Số tiền cọc kh&aacute;c nhau t&ugrave;y theo chương tr&igrave;nh m&agrave; qu&yacute; kh&aacute;ch t&ugrave;y chọn, số tiền c&ograve;n lại sẽ thanh to&aacute;n trước ng&agrave;y khởi h&agrave;nh l&agrave; 5 ng&agrave;y đến 15 ng&agrave;y t&ugrave;y theo loại h&igrave;nh Tour m&agrave; Qu&yacute; Kh&aacute;ch đ&atilde; chọn</p>\r\n\r\n<p>Việc thanh to&aacute;n xem l&agrave; ho&agrave;n tất khi HNTRAVELnhận được đủ tiền Tour &nbsp;trước ng&agrave;y khởi h&agrave;nh hoặc theo hợp đồng thỏa thuận giữa 2 b&ecirc;n. Bất kỳ việc thanh to&aacute;n chậm trễ dẫn đến việc hủy dịch vụ kh&ocirc;ng thuộc tr&aacute;ch nhiệm của HNTRAVEL.</p>\r\n\r\n<p>HNTRAVELsẽ kh&ocirc;ng giải quyết c&aacute;c trường hợp hệ thống tự động hủy phiếu đăng k&yacute; nếu qu&yacute; kh&aacute;ch kh&ocirc;ng thực hiện đ&uacute;ng những quy định tr&ecirc;n.</p>\r\n\r\n<p>Trường hợp bị bủy bỏ do HNTRAVEL:<br />\r\nNếu HNTRAVELkh&ocirc;ng thực hiện được chuyến du lịch, HNTRAVELphải b&aacute;o ngay cho kh&aacute;ch h&agrave;ng biết v&agrave; thanh to&aacute;n lại cho kh&aacute;ch h&agrave;ng to&agrave;n bộ số tiền m&agrave; kh&aacute;ch h&agrave;ng đ&atilde; đ&oacute;ng trong v&ograve;ng 3 ng&agrave;y kể từ l&uacute;c việc th&ocirc;ng b&aacute;o hủy chuyến đi du lịch bằng tiền mặt hoặc chuyển khoản.</p>\r\n\r\n<p>Trường hợp bị hủy v&eacute; do kh&aacute;ch h&agrave;ng:<br />\r\nĐối với ng&agrave;y thường:</p>\r\n\r\n<p>Huỷ trước 30 &ndash; 45 ng&agrave;y : 10% tổng gi&aacute; th&agrave;nh tour.<br />\r\nHuỷ trước 20 &ndash; 30ng&agrave;y : 20% tổng gi&aacute; th&agrave;nh tour<br />\r\nHuỷ trước 25 ng&agrave;y : 25% tổng gi&aacute; th&agrave;nh tour<br />\r\nHuỷ trước 15 ng&agrave;y : 30% tổng gi&aacute; th&agrave;nh tour<br />\r\nHuỷ trước 07 ng&agrave;y : 40% tổng gi&aacute; th&agrave;nh tour<br />\r\nHuỷ trước 03 &ndash; 06 ng&agrave;y : 75% tổng gi&aacute; th&agrave;nh tour.<br />\r\nSau thời gian tr&ecirc;n : 100% tổng gi&aacute; th&agrave;nh tour. Việc huỷ bỏ chuyến đi phải được th&ocirc;ng b&aacute;o trực tiếp với C&ocirc;ng ty hoặc qua fax, email v&agrave; phải được C&ocirc;ng ty x&aacute;c nhận. Việc huỷ bỏ bằng điện thoại sẽ kh&ocirc;ng được chấp nhận.</p>\r\n\r\n<p>Lưu &yacute;:</p>\r\n\r\n<p>&ndash; Trường hợp hủy tour do sự cố kh&aacute;ch quan như: thi&ecirc;n tai, khủng bổ, dịch bệnh,&hellip; HNTRAVELsẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường th&ecirc;m bất kỳ chi ph&iacute; n&agrave;o ngo&agrave;i việc ho&agrave;n trả tiền tour,</p>\r\n\r\n<p>&ndash; Tr&ecirc;n đ&acirc;y l&agrave; mức phạy hủy tối đa, chi ph&iacute; n&agrave;y c&oacute; thể được giảm t&ugrave;y theo điều kiện của từng nh&agrave; cung cấp dịch vụ cho HNTRAVEL.</p>\r\n\r\n<p>&ndash; Thời gian hủy chuyến du lịch được t&iacute;nh theo ng&agrave;y l&agrave;m việc (kh&ocirc;ng t&iacute;nh thứ 7, chủ nhật v&agrave; c&aacute;c ng&agrave;y lễ).</p>\r\n\r\n<p>&ndash; C&aacute;c điều kiện hủy v&eacute; như tr&ecirc;n&nbsp;kh&ocirc;ng &aacute;p dụng v&agrave;o dịp Lễ, Tết.</p>\r\n\r\n<p>Trường hợp bất khả kh&aacute;ng:<br />\r\nNếu chương tr&igrave;nh du lịch bị hủy bỏ hoặc thay đổi bởi một trong hai b&ecirc;n v&igrave; một l&yacute; do bất khả kh&aacute;ng (chiến tranh, thi&ecirc;n tai, khủng bố&hellip;), th&igrave; hai b&ecirc;n sẽ kh&ocirc;ng chịu bất kỳ nghĩa vụ bồi ho&agrave;n c&aacute;c tổn thất đ&atilde; xảy ra v&agrave; kh&ocirc;ng chịu bất tr&aacute;ch nhiệm nhiệm ph&aacute;p l&yacute; n&agrave;o. Tuy nhi&ecirc;n mỗi b&ecirc;n c&oacute; tr&aacute;ch nhiệm cố gắng tối đa để gi&uacute;p đỡ b&ecirc;n bị thiệt hại nhằm giảm thiểu c&aacute;c tổn thất g&acirc;y ra v&igrave; l&yacute; do bất khả kh&aacute;ng.</p>\r\n\r\n<p>IV)&nbsp;NHỮNG Y&Ecirc;U CẦU ĐẶC BIỆT TRONG CHUYẾN DU LỊCH</p>\r\n\r\n<p>C&aacute;c y&ecirc;u cầu đặc biệt của qu&yacute; kh&aacute;ch phải b&aacute;o trước cho HNTRAVEL ngay tại thời điểm đăng k&yacute;, HNTRAVEL cố gắng đ&aacute;p ứng những y&ecirc;u cầu n&agrave;y trong khả năng của m&igrave;nh song sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm về bất kỳ sự từ chối cung cấp dịch vụ từ ph&iacute;a c&aacute;c nh&agrave; vận chuyển, kh&aacute;ch sạn, nh&agrave; h&agrave;ng v&agrave; c&aacute;c nh&agrave; cung cấp dịch vụ độc lập kh&aacute;ch.</p>\r\n\r\n<p>VI)&nbsp;&nbsp;VẬN CHUYỂN</p>\r\n\r\n<p>Phương tiện vận chuyển t&ugrave;y theo từng chương tr&igrave;nh du lịch.</p>\r\n\r\n<p>Với chương tr&igrave;nh đi bằng xe: Xe m&aacute;y lạnh (4-7-15-25-35-45 chỗ) sẽ được HNTRAVELsắp xếp t&ugrave;y theo số lượng từng đo&agrave;n, phục vụ suốt chương tr&igrave;nh tham quan.</p>\r\n\r\n<p>Với chương tr&igrave;nh đi bằng xe lửa, m&aacute;y bay, t&agrave;u c&aacute;nh ngầm (phương tiện vận chuyển c&ocirc;ng cộng), trong một số chương tr&igrave;nh c&aacute;c nh&agrave; cung cấp dịch vụ c&oacute; thể thay đổi giời khởi h&agrave;nh m&agrave; kh&ocirc;ng b&aacute;o trước, việc thay đổi n&agrave;y sẽ được ETV th&ocirc;ng b&aacute;o cho kh&aacute;ch h&agrave;ng nếu thời gian cho ph&eacute;p.</p>\r\n\r\n<p>ETV kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi ho&agrave;n v&agrave; tr&aacute;ch nhiệm ph&aacute;p l&yacute; với những thiệt hại về vật chất lẫn tinh thần do việc chậm trễ giờ giấc khởi h&agrave;nh của c&aacute;c phương tiện vận chuyển c&ocirc;ng cộng hoặc sự chậm trễ do ch&iacute;nh h&agrave;nh kh&aacute;ch g&acirc;y ra. ETV chỉ thực hiện h&agrave;nh vi gi&uacute;p đỡ để giảm bớt tổn thất cho kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>VII)H&Agrave;NH L&Yacute;</p>\r\n\r\n<p>H&agrave;nh l&yacute; gọn nhẹ, với c&aacute;c chương tr&igrave;nh sử dụng dịch vụ h&agrave;ng kh&ocirc;ng h&agrave;nh l&yacute; miễn cước sẽ do c&aacute;c h&atilde;ng h&agrave;ng kh&ocirc;ng qui định.</p>\r\n\r\n<p>ETV kh&ocirc;ng chịu tr&aacute;ch nhiệm về sự thất lạc, hư hỏng h&agrave;nh l&yacute; hoặc bất kỳ vật dụng n&agrave;o của h&agrave;nh kh&aacute;ch trong suốt chuyến đi, du kh&aacute;ch tự bảo quản h&agrave;nh l&yacute; của m&igrave;nh. Nếu kh&aacute;ch h&agrave;ng mất hay thất lạc h&agrave;nh l&yacute; th&igrave; ETV sẽ gi&uacute;p h&agrave;nh kh&aacute;ch li&ecirc;n lạc v&agrave; khai b&aacute;o với c&aacute;c bộ phận li&ecirc;n quan để truy t&igrave;m h&agrave;nh l&yacute; bị mất hay thất lạc. Việc bồi thường h&agrave;nh l&yacute; mất hay thất lạc sẽ theo qui định của c&aacute;c đơn vị cung cấp dịch vụ hoặc c&aacute;c đơn vị bảo hiểm.</p>\r\n\r\n<p>IX, TIẾP NHẬN TH&Ocirc;NG TIN VỀ CHƯƠNG TR&Igrave;NH DU LỊCH</p>\r\n\r\n<p>Trước khi đăng k&yacute;, kh&aacute;ch h&agrave;ng vui l&ograve;ng đọc kỹ chương tr&igrave;nh, gi&aacute; v&eacute;, bao gồm cũng như kh&ocirc;ng bao gồm trong chương tr&igrave;nh. Kh&aacute;ch h&agrave;ng c&oacute; thể trực tiếp hoặc nhờ người đại diện đến đăng k&yacute; đi du lịch v&agrave; thanh to&aacute;n tiền v&eacute; tại c&aacute;c văn ph&ograve;ng, chi nh&aacute;nh của ETV. ETV chỉ c&oacute; tr&aacute;ch nhiệm cung cấp th&ocirc;ng tin chuyến đi cho kh&aacute;ch h&agrave;ng đến đăng k&yacute; trực tiếp hoặc cho người đại diện. ETV kh&ocirc;ng chịu bất cứ tr&aacute;ch nhiệm n&agrave;o trong trường hợp người đại diện kh&ocirc;ng cung cấp lại hoặc cung cấp kh&ocirc;ng ch&iacute;nh x&aacute;c c&aacute;c th&ocirc;ng tin của chuyến đi cho kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>X, TR&Aacute;CH NHIỆM V&Agrave; C&Aacute;C CAM KẾT KH&Aacute;C.</p>\r\n\r\n<p>Về ph&iacute;a HNTRAVEL<br />\r\n&ndash; Đảm bảo mọi dịch vụ theo đ&uacute;ng theo chương tr&igrave;nh.</p>\r\n\r\n<p>&ndash; Phổ biến đầy đủ c&aacute;c th&ocirc;ng tin cần thiết, c&aacute;c qui định khi đi du lịch trong v&agrave; ngo&agrave;i nước trước ng&agrave;y khởi h&agrave;nh</p>\r\n\r\n<p>&ndash; Với c&aacute;c chương tr&igrave;nh du lịch nước ngo&agrave;i, HNTRAVEL kh&ocirc;ng chịu tr&aacute;ch nhiệm về c&aacute;c h&agrave;nh kh&aacute;ch bị cơ quan hữu quan của nước ngo&agrave;i từ chối cho nhập cảnh. Mọi ph&aacute;t sinh từ việc từ chối n&agrave;y do kh&aacute;ch h&agrave;ng chi trả bao gồm cả chi ph&iacute; phạt hủy dịch vụ của c&aacute;c nh&agrave; cung cấp.</p>\r\n\r\n<p>Về ph&iacute;a kh&aacute;ch h&agrave;ng:<br />\r\n&ndash; Thanh to&aacute;n đầy đủ, đ&uacute;ng hạn.</p>\r\n\r\n<p>&ndash; Trong thời gian đi du lịch, kh&aacute;ch h&agrave;ng phải tu&acirc;n thủ theo chương tr&igrave;nh</p>\r\n\r\n<p>&ndash; Cung cấp hộ chiếu, h&igrave;nh ảnh v&agrave; c&aacute;c giấy tờ li&ecirc;n quan đến thủ tục xuất nhập cảnh đầy đủ, đ&uacute;ng hạng theo qui định</p>\r\n\r\n<p>&ndash; Tu&acirc;n thủ theo qui định v&agrave; ph&aacute;p luật c&aacute;c nước tham quan. HNTRAVELkh&ocirc;ng chịu tr&aacute;ch nhiệm ph&aacute;p l&yacute; cũng như vật chất trong trường hợp kh&aacute;ch h&agrave;ng vi phạm ph&aacute;p luật hoặc qui định của nước sở tại. Kh&aacute;ch h&agrave;ng phải chịu tr&aacute;ch nhiệm thanh to&aacute;n tất cả c&aacute;c chi ph&iacute; ph&aacute;t sinh do việc vi phạm g&acirc;y ra. HNTRAVELchỉ c&oacute; tr&aacute;ch nhiệm gi&uacute;p đỡ kh&aacute;ch h&agrave;ng trong trường hợp n&agrave;y nhằm giảm thiểu mức thiệt hại cho kh&aacute;ch.</p>\r\n\r\n<p>T&ugrave;y theo t&igrave;nh h&igrave;nh thực tế, HNTRAVELgiữ quyền thay đổi lộ tr&igrave;nh, sắp xếp lại thứ tự c&aacute;c điểm tham quan hoặc hủy bỏ chuyến đi du lịch bất cứ l&uacute;c n&agrave;o m&agrave; HNTRAVELthấy cần thiết v&igrave; sự thuận tiện hoặc an to&agrave;n cho kh&aacute;ch h&agrave;ng.<br />\r\nTrong qu&aacute; tr&igrave;nh thực hiện, nếu xảy ra tranh chấp sẽ được giải quyết tr&ecirc;n cơ sở thương lượng kh&ocirc;ng đạt được kết quả, vụ việc sẽ được đưa ra to&agrave;n &aacute;n theo đ&uacute;ng qui định của ph&aacute;p luật hiện h&agrave;nh. Mội chi ph&iacute; li&ecirc;n quan sẽ do b&ecirc;n thua kiện chịu./.<br />\r\n&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tour_instructor`
--

CREATE TABLE IF NOT EXISTS `tour_instructor` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `images` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `describe` text NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_instructor`
--

INSERT INTO `tour_instructor` (`id`, `fullname`, `birthday`, `images`, `email`, `phone_number`, `describe`, `status`) VALUES
(2, 'Nguyễn Văn H', '0000-00-00', 'bui_trung_h.jpg', 'animesuper7119@gmail.com', '0388240938', '<p>Nguyễn Văn H</p>\r\n', 1),
(3, 'Bùi Trung H', '0000-00-00', '3-nui-ham-lon.jpg', 'tuannqph08378@fpt.edu.vn', '0388240938', '<p>111</p>\r\n', 1),
(4, 'Nguyễn Thị T', '2019-12-06', 'hinh-nen-anime-dep-full-hd-2.jpg', 'admin@dmn', '0388240938', '<p>Nguyễn Thị T</p>\r\n', 1),
(6, 'Nguyễn Văn D', '2019-12-18', '5-nui-tram-768x577.jpg', 'dd@gmail.com', '0388240938', '<p>123333333333</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tour_list`
--

CREATE TABLE IF NOT EXISTS `tour_list` (
  `id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_instructor` int(11) NOT NULL,
  `tour_name` varchar(100) NOT NULL,
  `tour_cost` varchar(255) NOT NULL,
  `tour_sale` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `location` varchar(50) NOT NULL,
  `images` text NOT NULL,
  `short_desc` varchar(100) NOT NULL,
  `describe` text NOT NULL,
  `created_date` date NOT NULL,
  `update_at` date NOT NULL,
  `viewer` int(11) NOT NULL,
  `voting` tinyint(5) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `status_special` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_list`
--

INSERT INTO `tour_list` (`id`, `id_category`, `id_instructor`, `tour_name`, `tour_cost`, `tour_sale`, `duration`, `location`, `images`, `short_desc`, `describe`, `created_date`, `update_at`, `viewer`, `voting`, `date_start`, `date_end`, `status`, `status_special`) VALUES
(1, 1, 2, 'Quảng Trường Ba Đình 1', '87', '60', '3 ngày 2 đêm', 'hà nội', 'quang_truong_badinh.jpg', 'Lăng Bác là nơi lưu giữ thi hài của vị lãnh tụ kính yêu. Bên ngoài lăng là những hàng tre xanh bát n', '<p>Lăng B&aacute;c l&agrave; nơi lưu giữ thi h&agrave;i của vị l&atilde;nh tụ k&iacute;nh y&ecirc;u. B&ecirc;n ngo&agrave;i lăng l&agrave; những h&agrave;ng tre xanh b&aacute;t ng&aacute;t. Lăng chủ t&iacute;ch mở cửa v&agrave;o s&aacute;ng thứ 3,4,5,7 v&agrave; chủ nhật.&nbsp;C&aacute;c bạn lựa chọn kh&aacute;ch sạn&nbsp;để tiện cho việc tham quan cũng như đi chơi của m&igrave;nh. Khi v&agrave;o viếng lăng B&aacute;c, bạn ch&uacute; &yacute; ăn mặc chỉnh tề, kh&ocirc;ng đem theo c&aacute;c thiết bị điện tử ghi h&agrave;nh v&agrave; giữ trật tự trong lăng.</p>\r\n\r\n<p>C&aacute;c địa điểm du lịch nổi tiếng ở H&agrave; Nội: Lăng B&aacute;c v&agrave; quảng trường Ba Đ&igrave;nh (ảnh sưu tầm)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Quảng trường Ba Đ&igrave;nh l&agrave; quảng trường lớn nhất Việt Nam, nằm tr&ecirc;n đường H&ugrave;ng Vương v&agrave; trước Lăng Chủ tịch Hồ Ch&iacute; Minh. Quảng trường n&agrave;y c&ograve;n l&agrave; nơi ghi nhận nhiều dấu ấn quan trọng trong lịch sử Việt Nam. Đặc biệt, v&agrave;o ng&agrave;y 2 th&aacute;ng 9 năm 1945, Chủ tịch Ch&iacute;nh phủ C&aacute;ch mạng l&acirc;m thời Việt Nam D&acirc;n chủ Cộng h&ograve;a Hồ Ch&iacute; Minh đ&atilde; đọc bản Tuy&ecirc;n ng&ocirc;n độc lập khai sinh ra nước Việt Nam D&acirc;n chủ Cộng h&ograve;a. Đ&acirc;y cũng l&agrave; nơi diễn ra c&aacute;c cuộc diễu h&agrave;nh nh&acirc;n dịp c&aacute;c ng&agrave;y lễ lớn của Việt Nam, v&agrave; cũng l&agrave; một địa điểm tham quan, vui chơi, dạo m&aacute;t của du kh&aacute;ch v&agrave; người d&acirc;n H&agrave; Nội.</p>\r\n', '2019-12-03', '2019-12-10', 0, 5, '2019-12-03', '2019-12-06', 1, 1),
(2, 1, 3, 'Hồ Gươm', '100', '80', '3 ngày 2 đêm', 'hà nội', 'ho_guom.jpg', 'Hồ Gươm hay hồ Hoàn Kiếm là một trong những nơi nên đến ở Hà Nội khi du lịch thủ đô.', '<p>Hồ Gươm hay hồ Ho&agrave;n Kiếm l&agrave; một trong&nbsp;<strong>những nơi n&ecirc;n đến&nbsp;ở H&agrave; Nội</strong>&nbsp;khi du lịch thủ đ&ocirc;.&nbsp;Nằm ở giữa trung t&acirc;m, Hồ Gươm&nbsp;được v&iacute; như tr&aacute;i tim của th&agrave;nh phố ng&agrave;n năm tuổi n&agrave;y.. Mặt hồ như tấm gương lớn soi b&oacute;ng những c&acirc;y cổ thụ, những rặng liễu thướt tha t&oacute;c rủ, những m&aacute;i đền, ch&ugrave;a cổ k&iacute;nh, th&aacute;p cũ r&ecirc;u phong, c&aacute;c to&agrave; nh&agrave; mới cao tầng vươn l&ecirc;n trời xanh.</p>\r\n\r\n<p>Một trải nghiệm th&uacute; vị d&agrave;nh cho kh&aacute;ch du lịch l&agrave; đi bộ một v&ograve;ng hồ, bạn sẽ được thấy một H&agrave; Nội cổ k&iacute;nh nhưng vẫn đầy hiện đại hiện l&ecirc;n thật r&otilde; r&agrave;ng. B&ecirc;n cạnh hồ l&agrave; những c&ocirc;ng tr&igrave;nh kiến tr&uacute;c như th&aacute;p B&uacute;t, đ&agrave;i Nghi&ecirc;n, cầu Th&ecirc; H&uacute;c dẫn v&agrave;o đền Ngọc Sơn, đền vua L&ecirc; Th&aacute;i Tổ, th&aacute;p Ho&agrave; Phong,&hellip;ướt tha t&oacute;c rủ, những m&aacute;i đền, ch&ugrave;a cổ k&iacute;nh, th&aacute;p cũ r&ecirc;u phong, c&aacute;c to&agrave; nh&agrave; mới cao tầng vươn l&ecirc;n trời xanh.</p>\r\n', '2019-12-03', '2019-12-03', 0, 5, '2019-12-07', '2019-12-10', 1, 1),
(3, 1, 3, 'Núi Hàm Lợn', '87', '80', '5 ngày 2 đêm', 'Hà nội', '1-nui-ham-lon.jpg', 'Nằm trong quần thể dãy núi Độc Tôn – Sóc Sơn, với độ cao khoảng 462m', '<p>N&uacute;i H&agrave;m Lợn kh&ocirc;ng c&ograve;n l&agrave; c&aacute;i t&ecirc;n xa lạ mỗi khi c&aacute;c &ldquo;phượt thủ&rdquo; nhắc đến c&aacute;c<strong>&nbsp;<a href="https://www.vntrip.vn/cam-nang/dia-diem-phuot-gan-ha-noi-11562" target="_blank">địa điểm phượt gần H&agrave; Nội</a>.</strong>&nbsp;Theo đường cao tốc Thăng Long &ndash; Nội B&agrave;i hướng đi Vĩnh Ph&uacute;c, chỉ hơn một tiếng l&agrave; c&aacute;c bạn đ&atilde; đến với điểm du lịch n&agrave;y.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nằm trong quần thể d&atilde;y n&uacute;i Độc T&ocirc;n &ndash; S&oacute;c Sơn, với độ cao khoảng 462m, H&agrave;m Lợn l&agrave; đỉnh n&uacute;i đầy th&aacute;ch thức với c&aacute;c phượt thủ.</p>\r\n\r\n<p>Ở H&agrave;m Lợn, bạn ho&agrave;n to&agrave;n c&oacute; thể cắm trại qua đ&ecirc;m b&ecirc;n hồ N&uacute;i B&agrave;u rộng m&ecirc;nh m&ocirc;ng dưới ch&acirc;n n&uacute;i. Kh&ocirc;ng kh&iacute; n&uacute;i&nbsp;rừng m&aacute;t mẻ cộng th&ecirc;m gi&oacute; hồ phả v&agrave;o n&ecirc;n d&ugrave; tiết trời oi bức th&igrave; th&igrave; H&agrave;m Lợn vẫn lu&ocirc;n tho&aacute;ng m&aacute;t, trong l&agrave;nh. Th&ecirc;m v&agrave;o đ&oacute;, với mức gi&aacute; rẻ, H&agrave;m Lợn ch&iacute;nh l&agrave;<strong>&nbsp;điểm du lịch gần H&agrave; Nội</strong>&nbsp;h&agrave;ng đầu m&agrave; nhiều nh&oacute;m sinh vi&ecirc;n lựa chọn.</p>\r\n\r\n<p>B&ecirc;n bờ hồ N&uacute;i B&agrave;u (ảnh: Phương Mai)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tuy nhi&ecirc;n, những cơn mưa bất chợt ở đ&acirc;y c&oacute; lẽ sẽ g&acirc;y ra đ&ocirc;i ch&uacute;t trở ngại cho việc nấu nướng. V&igrave; thế, c&aacute;c vị kh&aacute;ch du lịch n&ecirc;n chuẩn bị ch&uacute;t đồ ăn ở nh&agrave; để đề ph&ograve;ng. Hoặc bạn cũng c&oacute;&nbsp;thể&nbsp;<a href="https://www.vntrip.vn/khach-san/ha-noi/soc-son?utm_source=Blog&amp;utm_campaign=Blog_0308_Optimize_mbac_3215_diadiemganhanoi" target="_blank">đặt ph&ograve;ng&nbsp;</a><a href="https://www.vntrip.vn/">kh&aacute;ch sạn</a>&nbsp;ở S&oacute;c Sơn&nbsp;đ&atilde; được chuẩn bị sẵn đồ ăn v&agrave; ph&ograve;ng nghỉ ngơi đầy đủ nh&eacute;.</p>\r\n\r\n<blockquote>&nbsp;</blockquote>\r\n', '2019-12-06', '2019-12-09', 0, 4, '2019-12-06', '2019-12-31', 1, 1),
(4, 3, 3, 'Núi Trầm', '100', '80', '5 ngày 2 đêm', 'Hà nội', 'nui-tram-vntrip1-e1533285295428.jpg', 'Chỉ cách trung tâm thủ đô tầm 25km, Núi Trầm và di tích chùa Trầm đã trở thành một điểm du lịch gần ', '<p>&nbsp;</p>\r\n\r\n<p>Chỉ c&aacute;ch trung t&acirc;m thủ đ&ocirc; tầm 25km, N&uacute;i Trầm v&agrave; di t&iacute;ch ch&ugrave;a Trầm đ&atilde; trở th&agrave;nh một điểm du lịch gần H&agrave; Nội được kh&ocirc;ng &iacute;t bạn trẻ th&iacute;ch d&atilde; ngoại trekking lựa chọn.</p>\r\n\r\n<p>V&igrave; điểm du lịch n&agrave;y nằm ở cửa ng&otilde; Thủ đ&ocirc; n&ecirc;n phương tiện đi tới đ&acirc;y cũng rất linh hoạt. Bạn c&oacute; thể đi xe đạp địa h&igrave;nh, xe m&aacute;y v&agrave; kể cả xe bu&yacute;t. Với phong cảnh đẹp h&ugrave;ng vĩ, n&uacute;i Trầm hứa hẹn l&agrave; nơi chụp ảnh đẹp cho tất cả mọi người.</p>\r\n', '2019-12-06', '2019-12-10', 0, 4, '2019-12-06', '2019-12-31', 1, 0),
(5, 3, 6, 'Đền Ngọc Sơn', '1000', '800', '5 ngày 2 đêm', 'Hà nội', 'vuon-quoc-gia-ba-vi-vntrip1-e1533285584906.jpg', 'Cầu Thê Húc màu đỏ son, cong cong dẫn vào đền Ngọc Sơn ẩn sau những bóng đa cổ thụ. Đền được xây dựn', '<p>Cầu Th&ecirc; H&uacute;c m&agrave;u đỏ son, cong cong dẫn v&agrave;o đền Ngọc Sơn ẩn sau những b&oacute;ng đa cổ thụ. Đền được x&acirc;y dựng v&agrave;o thế kỉ 19 v&agrave; l&agrave; c&ocirc;ng tr&igrave;nh điển h&igrave;nh về kh&ocirc;ng gian v&agrave; tạo t&aacute;c kiến tr&uacute;c. Với sự kết hợp h&agrave;i ho&agrave; giữa đền v&agrave; hồ, Đền Ngọc Sơn c&ugrave;ng với Hồ Ho&agrave;n Kiếm đ&atilde; tạo n&ecirc;n một tổng thể kiến tr&uacute;c Thi&ecirc;n &ndash; Nh&acirc;n hợp nhất, mang lại một kh&ocirc;ng gian chan ho&agrave; giữa thi&ecirc;n nhi&ecirc;n v&agrave; con người.</p>\r\n\r\n<p>Người d&acirc;n c&ugrave;ng nhiều du kh&aacute;ch khi dạo bước b&ecirc;n Hồ Gươm vẫn thường hay đi v&agrave;o Đền Ngọc Sơn thắm n&eacute;n hương v&agrave; nguyện cầu những điều tốt đẹp. Đền Ngọc Sơn, c&ugrave;ng với Hồ Ho&agrave;n Kiếm, Th&aacute;p R&ugrave;a, Cầu Th&ecirc; H&uacute;c, Th&aacute;p B&uacute;t, Đ&agrave;i Nghi&ecirc;n đ&atilde; trở th&agrave;nh một cụm di t&iacute;ch lịch sử v&agrave;&nbsp;<strong>danh lam thắng cảnh H&agrave; Nội</strong>&nbsp;mang n&eacute;t kiến tr&uacute;c đặc sắc, ti&ecirc;u biểu m&agrave; du kh&aacute;ch kh&ocirc;ng thể bỏ qua khi đến thăm thủ đ&ocirc;.</p>\r\n', '2019-12-07', '2019-12-07', 0, 5, '2019-12-07', '2019-12-31', 1, 0),
(6, 2, 6, 'Phố cổ', '89', '80', '4 ngày 3 đêm', 'Hà nội', 'phoco.jpg', ' Phố cổ Hà Nội – địa điểm du lịch hấp dẫn', '<p>Muốn t&igrave;m hiểu về cuộc sống, văn h&oacute;a v&agrave; con người Tr&agrave;ng An th&igrave; bạn đừng bỏ qua phố cổ&nbsp;&ndash; một trong những&nbsp;<strong>địa điểm du lịch ở H&agrave; Nội</strong>&nbsp;đầy th&uacute; vị v&agrave; hấp dẫn với du kh&aacute;ch. Phố cổ H&agrave; Nộ<a href="https://www.vntrip.vn/cam-nang/pho-co-ha-noi-12944">i</a>&nbsp;nằm ở ph&iacute;a T&acirc;y v&agrave; ph&iacute;a Bắc của Hồ Ho&agrave;n Kiếm, l&agrave; nơi tập trung đ&ocirc;ng d&acirc;n cư sinh sống c&oacute; 36 phố phường. Mỗi con phố ở đ&acirc;y chủ yếu tập trung b&aacute;n một loại mặt h&agrave;ng nhất định. Lang thang ở khu phố v&agrave; thưởng thức ẩm thực phố cổ như phở B&aacute;t Đ&agrave;n, chả c&aacute; L&atilde; Vọng, b&uacute;n chả h&agrave;ng M&agrave;nh, m&igrave; vằn thắn Đinh Liệt, b&uacute;n ốc nguội &Ocirc; Quan Chưởng,&hellip;sẽ khiến chuyến đi của bạn đ&aacute;ng nhớ hơn rất nhiều! Lựa chọn nhiều&nbsp;<a href="https://www.vntrip.vn/khach-san/ha-noi/pho-co-ha-noi" target="_blank">kh&aacute;ch sạn Phố Cổ</a>&nbsp;để tận hưởng ẩm thực nơi đ&acirc;y.</p>\r\n', '2019-12-09', '2019-12-09', 0, 5, '2019-12-09', '2019-12-31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `password`, `email`, `phone_number`, `role`, `status`) VALUES
(1, 'tuan', 'nguyen', 'admin', '202cb962ac59075b964b07152d234b70', 'animesuper7119@gmail.com', '0388240938', 0, 1),
(2, 'chido', 'nguyen', 'user1', 'ee11cbb19052e40b07aac0ca060c23ee', 't@gmail.com', '555555555555', 1, 1),
(3, 'chido1', 'nguyen', 'user2', 'ee11cbb19052e40b07aac0ca060c23ee', 't@gmail.com', '555555555555', 1, 1),
(4, 'chido3', 'nguyen', 'user3', 'ee11cbb19052e40b07aac0ca060c23ee', 't@gmail.com', '555555555555', 1, 1),
(5, 'chido', 'nguyen', 'admin1', 'ee11cbb19052e40b07aac0ca060c23ee', 'animesuper7119@gmail.com', '0388240938', 1, 1),
(11, 'Tuấn123', 'Nguyễn', 'tuan299', '202cb962ac59075b964b07152d234b70', 'animesuper7119@gmail.com', '0388240938', 1, 1),
(12, 'tuan', 'nguyen', 'user29', '202cb962ac59075b964b07152d234b70', 'tuan@gmail.com', '033333333', 1, 1),
(13, 't', 'nguyen', 't123', '202cb962ac59075b964b07152d234b70', 't@gmail.com', '038824022', 1, 1),
(15, 'N', 'T', 'nt2', '202cb962ac59075b964b07152d234b70', 'animesuper7119@gmail.com', '0388240938', 1, 1),
(16, 'user100', 'test', 'user100', '202cb962ac59075b964b07152d234b70', 'user100@gmail.com', '1234567890', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `video_slide`
--

CREATE TABLE IF NOT EXISTS `video_slide` (
  `name` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `video` text NOT NULL,
  `describe` text NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `video_slide`
--

INSERT INTO `video_slide` (`name`, `images`, `video`, `describe`, `id`) VALUES
('video slide tour', 'anhbia2.jpg', 'banner2.mp4', '<p>T&igrave;m chuyến đi đặc biệt ngay h&ocirc;m nay với</p>\r\n\r\n<h2>Travel Tours</h2>\r\n', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album_list`
--
ALTER TABLE `album_list`
  ADD PRIMARY KEY (`id`), ADD KEY `id_tour` (`id_tour`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cmt`
--
ALTER TABLE `cmt`
  ADD PRIMARY KEY (`id`), ADD KEY `id_tour` (`id_tour`), ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`), ADD KEY `id_tour` (`id_tour`), ADD KEY `id_instructor` (`id_instructor`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_instructor`
--
ALTER TABLE `tour_instructor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_list`
--
ALTER TABLE `tour_list`
  ADD PRIMARY KEY (`id`), ADD KEY `id_category` (`id_category`), ADD KEY `id_instructor` (`id_instructor`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_slide`
--
ALTER TABLE `video_slide`
  ADD PRIMARY KEY (`name`), ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album_list`
--
ALTER TABLE `album_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cmt`
--
ALTER TABLE `cmt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tour_instructor`
--
ALTER TABLE `tour_instructor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tour_list`
--
ALTER TABLE `tour_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `album_list`
--
ALTER TABLE `album_list`
ADD CONSTRAINT `album_list_ibfk_1` FOREIGN KEY (`id_tour`) REFERENCES `tour_list` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cmt`
--
ALTER TABLE `cmt`
ADD CONSTRAINT `cmt_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `cmt_ibfk_3` FOREIGN KEY (`id_tour`) REFERENCES `tour_list` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_list`
--
ALTER TABLE `order_list`
ADD CONSTRAINT `order_list_ibfk_3` FOREIGN KEY (`id_tour`) REFERENCES `tour_list` (`id`);

--
-- Constraints for table `tour_list`
--
ALTER TABLE `tour_list`
ADD CONSTRAINT `tour_list_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`),
ADD CONSTRAINT `tour_list_ibfk_2` FOREIGN KEY (`id_instructor`) REFERENCES `tour_instructor` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
