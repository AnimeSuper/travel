
function validateFileType() {
    var file = document.getElementById("imgInp");
    var fileName = file.value,
        idxDot = fileName.lastIndexOf(".") + 1,
        extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

    if (extFile == "jpg" || extFile == "jpeg" || extFile == "png") {
    }
    else {
        alert("Only jpg/jpeg and png files are allowed!");
        file.value = "";  // Reset the input so no files are uploaded
    }

    if (file.files[0].size > 30720000) {
        alert("This file is too big");
        file.value = "";  // Reset the input so no files are uploaded
    }
}

