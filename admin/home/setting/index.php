<!DOCTYPE html>
<html>

<head>

    <?php
    global $cdUpRefArray;
    include "../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php");


    ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content">
            <div class="row">
                <?php $result = getAllSetting(); ?>
                <div class="col-xs-12">
                    <div class="box">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="box-header">
                                <h3 class="box-title">Logo</h3>
                            </div>
                            <div class="input-group">
                                <input type="file" onchange="validateFileType();" class="form-control-file"
                                       name="images"
                                       id="imgInp">
                                <img width="200px" id="blah"
                                     src="<?php echo globalUrl($cdUpRefArray) ?>../images/logo/<?php echo $result['logo'] ?>"
                                     alt="your image">

                            </div>
                            <div class="box-header">
                                <h3 class="box-title">Tour Travel</h3>
                            </div>

                            <div class="box-header with-border">
                                <h3 class="box-title">Website* :</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['website'] ?>'
                                           class="form-control"
                                           name="website">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Phone Number* :</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['phone_number'] ?>'
                                           class="form-control"
                                           name="phone_number">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Email * :</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['email'] ?>'
                                           class="form-control"
                                           name="email">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Address *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['address'] ?>'
                                           class="form-control"
                                           name="address">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Map *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['map'] ?>'
                                           class="form-control"
                                           name="map">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Work Time *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['work_time'] ?>'
                                           class="form-control"
                                           name="work_time">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Diverse Destinations *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text"
                                           value='<?php echo $result['diverse_destinations'] ?>'
                                           class="form-control"
                                           name="diverse_destinations">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Value Of Money *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['value_of_money'] ?>'
                                           class="form-control"
                                           name="value_of_money">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Nice Place *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['nice_place'] ?>'
                                           class="form-control"
                                           name="nice_place">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Travel Enthusiast *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <input width="100%" type="text" value='<?php echo $result['travel_enthusiasts'] ?>'
                                           class="form-control"
                                           name="travel_enthusiasts">
                                </div>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Terms of service *:</h3>
                                <div class="input-group">
                                    <span class="input-group-addon"></span>
                                    <div style="padding: 0 !important;" class="box-body pad">

                    <textarea required id="editor1" name="editor1" rows="10" cols="80"
                              style="visibility: hidden; display: none;"><?php echo $result['terms_of_service'] ?>
                    </textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-header -->

                            <button style="margin: 20px;" onclick="return confirm('Có chắc muốn cập nhật')"
                                    type="submit" name="btn_save" class="btn btn-default">Save
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>

<!-- /.box -->
</div>

<!-- /.col -->
</section>
<?php
if (isset($_POST['btn_save'])) {

    $images = $_FILES['images']['name'];
    if (isset($images) && !empty($images)) {
        $images = $_FILES['images']['name'];
        $tmpA_cate = $_FILES['images']['tmp_name'];
        move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/logo/" . $images);
    } else {
        $images = $result['logo'];

    }
    $website = $_POST['website'];
    $phone_number = $_POST['phone_number'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $map = $_POST['map'];
    $work_time = $_POST['work_time'];
    $diverse_destinations = $_POST['diverse_destinations'];
    $value_of_money = $_POST['value_of_money'];
    $nice_place = $_POST['nice_place'];
    $travel_enthusiast = $_POST['travel_enthusiasts'];
    $rule = $_POST['editor1'];
    $sql = "UPDATE `settings` SET 
`website`='$website',
`phone_number`='$phone_number',
`email`='$email',
`address`='$address',`map`='$map',
`work_time`='$work_time',
`diverse_destinations`='$diverse_destinations',
`value_of_money`='$value_of_money',
`nice_place`='$nice_place',
`travel_enthusiasts`='$travel_enthusiast',
`logo`='$images',
terms_of_service='$rule';
 WHERE id=1 ";

    $settings = $conn->exec($sql);
    if ($settings == 1) {
        echo "<script>alert('Update successfull')</script>";

        header("Refresh:0");
    } else {
        echo "<script>alert('Update successfull')</script>";

        header("Refresh:0");
    }


}
?>

<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>

<!-- Control Sidebar -->
<?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<!-- ./wrapper -->

<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>