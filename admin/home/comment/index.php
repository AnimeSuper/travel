<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;
    include "../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php"); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tour-Travel
                <small>All Comment</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo globalUrl($cdUpRefArray); ?>home/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Tour-Travel</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6">

                                    </div>
                                    <div class="col-sm-6">
                                        <div id="example1_filter" class="dataTables_filter">

                                            <label>Search:
                                                <input type="search" name="search_text" id="search_text" placeholder=""
                                                       class="form-control  input-sm"/>
                                                <span id="search_btn" class="btn btn-primary">Search</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <img src="" alt="">
                                <div class="row">
                                    <div id="result" class="col-sm-12 ">
                                        <table id="example1" class="table table-bordered table-striped dataTable"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending" style="width: 297px;">
                                                    #
                                                </th>


                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;">Product
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;"><i class="fa fa-fw fa-eye"></i>
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;">Latest</i>
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;">Oldest</i>
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 190px;">Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $detail_sql = "SELECT * FROM `tour_list`";
                                            $details = $conn->query($detail_sql)->fetchAll();
                                            foreach ($details as $key => $detail_row) { ?>
                                                <tr role="row" class="<?php if ($key % 1) echo "odd";
                                                else echo "even"; ?>">
                                                    <td class="sorting_1"><?php echo($key + 1); ?></td>

                                                    <td><?php echo $detail_row['tour_name'] ?></td>

                                                    <td>
                                                        <?php
                                                        $cmt_product = $detail_row['id'];
                                                        $all_cmt = "SELECT COUNT(comment)  AS all_cmt FROM `cmt` WHERE id_tour=$cmt_product";

                                                        $all_cmt = $conn->query($all_cmt)->fetch();
                                                        echo $all_cmt['all_cmt'] ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $cmt_product = $detail_row['id'];
                                                        $all_cmt = "SELECT time_post  AS time FROM `cmt` WHERE id_tour=$cmt_product order by time_post" . " desc limit 1";

                                                        $all_cmt = $conn->query($all_cmt)->fetch();
                                                        $date = date_create($all_cmt['time']);
                                                        if (isset($all_cmt['time'])) {
                                                            echo date_format($date, "d/m/Y");
                                                        } else {
                                                            $date1 = ("0/0/0000");
                                                            echo $date1;
                                                        } ?>
                                                    </td>
                                                    <td>
                                                        <?php

                                                        $cmt_product = $detail_row['id'];
                                                        $all_cmt = "SELECT time_post  AS time FROM `cmt` WHERE id_tour=$cmt_product order by time_post" . " asc limit 1";
                                                        $all_cmt = $conn->query($all_cmt)->fetch();
                                                        $date = date_create($all_cmt['time']);
                                                        if (isset($all_cmt['time'])) {
                                                            echo date_format($date, "d/m/Y");
                                                        } else {
                                                            $date1 = ("0/0/0000");
                                                            echo $date1;
                                                        } ?>
                                                    </td>
                                                    <td>

                                                        <div class="btn-group">
                                                            <a href="<?php globalUrl($cdUpRefArray) ?>detail/?id=<?php echo $detail_row['id'] ?>">
                                                                <button
                                                                        type="submit" class="btn btn-primary">See
                                                                    details
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th rowspan="1" colspan="1">#</th>

                                                <th rowspan="1" colspan="1">Product</th>

                                                <th rowspan="1" colspan="1"><i class="fa fa-fw fa-eye"></i></th>

                                                <th rowspan="1" colspan="1">Latest</th>
                                                <th rowspan="1" colspan="1">Oldest</th>
                                                <th rowspan="1" colspan="1"> Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="dataTables_info" id="example1_info" role="status"
                                             aria-live="polite">Showing 1 to 10 of 57 entries
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button previous disabled" id="example1_previous"><a
                                                            href="#" aria-controls="example1" data-dt-idx="0"
                                                            tabindex="0">Previous</a>
                                                </li>
                                                <li class="paginate_button active"><a href="#" aria-controls="example1"
                                                                                      data-dt-idx="1" tabindex="0">1</a>
                                                </li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="2" tabindex="0">2</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="3" tabindex="0">3</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="4" tabindex="0">4</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="5" tabindex="0">5</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="6" tabindex="0">6</a></li>
                                                <li class="paginate_button next" id="example1_next"><a href="#"
                                                                                                       aria-controls="example1"
                                                                                                       data-dt-idx="7"
                                                                                                       tabindex="0">Next</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>

    <!-- Control Sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<!-- ./wrapper -->

<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>