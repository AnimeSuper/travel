<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;
    include "../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php"); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content">
            <div class="row">
                <?php $videos = videoSlide(); ?>
                <div class="col-xs-12">
                    <div class="box">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="box-header">
                                <h3 class="box-title">Tour Travel</h3>
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Poster:</h3>
                            </div>
                            <!-- /.box-header -->

                            <div class="input-group">

                                <span class="input-group-addon"><i class="fa fa-fw fa-file-picture-o"></i></span>


                                <input type="file" onchange="validateFileType();" class="form-control-file"
                                       name="images"
                                       id="imgInp">
                                <img width="200px" id="blah"
                                     src="<?php echo globalUrl($cdUpRefArray) ?>../images/banner/<?php echo $videos['images'] ?>"
                                     alt="your image">

                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Name:</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="input-group">

                                <span class="input-group-addon"><i class="fa fa-fw fa-google-plus-square"></i></span>
                                <input type="text" name="name" value="<?php echo $videos['name'] ?>"
                                       class="form-control">
                            </div>
                            <div class="box-header with-border">
                                <h3 class="box-title">Video:</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="">
                                <input type="file"
                                       class="form-control-file"
                                       name="video">
                                <iframe align="middle" frameborder="1" width="100%" height="517"
                                        src="<?php echo globalUrl($cdUpRefArray) ?>../images/banner/<?php echo $videos['video'] ?>">

                                </iframe>


                                <div class="box-header with-border">
                                    <h3 class="box-title">Describe</h3>
                                </div>
                                <div style="padding: 0" class="col-xs-12">


                    <textarea id="editor1" name="describe" rows="10" cols="80"
                              style="visibility: hidden; display: none;">                    <?php echo $videos['describe'] ?>
                    </textarea>


                                </div>
                                <div class="box-footer">
                                    <button type="submit" name="btn_save" class="btn btn-default">Save</button>

                                </div>

                        </form>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>

<!-- /.box -->
</div>

<!-- /.col -->
</section>
<?php
if (isset($_POST['btn_save'])) {
    $name = $_POST['name'];

    $images = $_FILES['images']['name'];
    if (isset($images) && !empty($images)) {
        $images = $_FILES['images']['name'];
        $tmpA_cate = $_FILES['images']['tmp_name'];
        move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/banner/" . $images);
    } else {
        $images = $videos['images'];

    }
    $video = $_FILES['video']['name'];
    if (isset($video) && !empty($video)) {
        $video = $_FILES['video']['name'];
        $tmpA_cate = $_FILES['video']['tmp_name'];
        move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/banner/" . $video);
    } else {
        $video = $videos['video'];

    }
    $describe = $_POST['describe'];
    $sql = "UPDATE `video_slide` SET `name`='$name',`images`='$images',`video`='$video',`describe`='$describe' WHERE id=0";

    $result = $conn->exec($sql);
    if ($result == 1) {

        header("Location:" . globalUrl($cdUpRefArray) . "home/video_slide/");
    } else {
        echo "<script>alert('Date Not change')</script>";
        header("Location:" . globalUrl($cdUpRefArray) . "home/video_slide/");

    }
}
?>

<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>

<!-- Control Sidebar -->
<?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<!-- ./wrapper -->

<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>