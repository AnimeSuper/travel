<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;
    include "../../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php"); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tour-Travel
                <small>All Member</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo globalUrl($cdUpRefArray); ?>home/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <!--Add New Product -->

        <a href="<?php echo globalUrl($cdUpRefArray) ?>home/user/member/create/">
            <button type="button" style="width: 10%; float: right; margin: 15px ;" class="btn btn-block btn-info">
                Add New Member
            </button>
        </a>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Tour-Travel</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6">

                                    </div>
                                    <div class="col-sm-6">
                                        <div id="example1_filter" class="dataTables_filter">

                                            <label>Search:
                                                <input type="search" name="search_text" id="search_text" placeholder=""
                                                       class="form-control  input-sm"/>
                                                <span id="search_btn" class="btn btn-primary">Search</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <img src="" alt="">
                                <div class="row">
                                    <div id="result" class="col-sm-12 ">
                                        <?php $members=getAllMember()?>
                                        <table style="" id="example1"
                                               class="table table-bordered table-striped dataTable"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending" style="width: 297px;">
                                                    #
                                                </th>

                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 322px;">FirstName
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;">Last Name
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;">User Name
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;">Email
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="width: 257px;">Phone Number
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="text-align: center;">Role
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                                                    colspan="1" style="text-align: center;">Status
                                                </th>

                                                <th class="sorting" style="text-align: center;" tabindex="0"
                                                    aria-controls="example1" rowspan="1"
                                                    colspan="2" style="">Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php global $cdUpRefArray;
                                            foreach ($members as $key => $rows) { ?>

                                                <tr role="row" class="<?php if ($key % 1) echo "odd";
                                                else echo "even"; ?>">
                                                    <td class="sorting_1"><?php echo($key + 1); ?></td>
                                                    <td><?php echo $rows['first_name'] ?>
                                                        <br>
                                                        Id: <?php echo $rows['id'] ?>
                                                    </td>
                                                    <td><?php echo $rows['last_name'] ?>

                                                    </td>

                                                    </td>
                                                    <td> <?php echo $rows['username'] ?></td>
                                                    <td> <?php echo $rows['email'] ?></td>
                                                    <td> <?php echo $rows['phone_number'] ?></td>
                                                    <td> <?php if($rows['role']==1){
                                                        echo"User";
                                                        }else{
                                                        echo "Admin";
                                                        } ?></td>
                                                    <td align="center">
                    <span class="label  label-<?php if ($rows['status'] == 1) {
                        echo "primary";
                    } else {
                        echo "danger";
                    } ?>"><?php if ($rows['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Shut Down";
                        } ?></span>
                                                    </td>

                                                    <td style="text-align: center;">
                                                        <div class="btn-group ">
                                                            <a href="<?php echo globalUrl($cdUpRefArray); ?>home/user/member/action/edit.php?id=<?php echo $rows['id'] ?>">
                                                                <button type="submit" class="btn btn-info">Edit</button>
                                                            </a>
                                                        </div>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <div class="btn-group">
                                                            <a href="<?php echo globalUrl($cdUpRefArray); ?>home/user/member/action/delete.php?id=<?php echo $rows['id'] ?>">
                                                                <button onclick="return confirm('Delete User!')"
                                                                        type="submit" class="btn btn-danger">Delete
                                                                </button>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php }
                                            ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th rowspan="1" colspan="1">#</th>
                                                <th rowspan="1" colspan="1">First Name</th>
                                                <th rowspan="1" colspan="1">Last Name</th>
                                                <th rowspan="1" colspan="1">User Name</th>

                                                <th rowspan="1" colspan="1">Email</th>
                                                <th rowspan="1" colspan="1">Phone Number</th>
                                                <th rowspan="1" colspan="1">Role</th>
                                                <th style="text-align: center;" rowspan="1" colspan="1">Status</th>
                                                <th style="text-align: center;" rowspan="1" colspan="2"> Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <?php
                                        ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="dataTables_info" id="example1_info" role="status"
                                             aria-live="polite">Showing 1 to 10 of 57 entries
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button previous disabled" id="example1_previous"><a
                                                            href="#" aria-controls="example1" data-dt-idx="0"
                                                            tabindex="0">Previous</a>
                                                </li>
                                                <li class="paginate_button active"><a href="#" aria-controls="example1"
                                                                                      data-dt-idx="1" tabindex="0">1</a>
                                                </li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="2" tabindex="0">2</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="3" tabindex="0">3</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="4" tabindex="0">4</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="5" tabindex="0">5</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="6" tabindex="0">6</a></li>
                                                <li class="paginate_button next" id="example1_next"><a href="#"
                                                                                                       aria-controls="example1"
                                                                                                       data-dt-idx="7"
                                                                                                       tabindex="0">Next</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>

    <!-- Control Sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<!-- ./wrapper -->

<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>