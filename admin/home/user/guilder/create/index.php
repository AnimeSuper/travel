<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;
    include "../../../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php");
    ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tour-Travel
                <small>Create Instructor</small>
            </h1>

        </section>

        <div style="margin: 20px;" class="box box-primary">
            <div class="box-header with-border">

            </div>
            <?php echo createInstructor(); ?>
            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-xs-2">
                        <br>
                        <div class="form-group">
                            <label for="imgInp">Choose Image</label>
                            <br>
                            <img width="100px" id="blah" src="<?php echo globalUrl($cdUpRefArray) ?>home/noimage.png"
                                 alt="your image"/>
                            <br>
                            <input required type="file" onchange="validateFileType();" class="form-control-file"
                                   name="images" id="imgInp">
                        </div>
                        <br>
                    </div>
                    <div class="col-xs-10">
                        <div class="col-xs-12">
                            <br>
                            <label>Full Name</label>
                            <input type="text" required class="form-control" name="fullname"
                                   placeholder="Full Name">
                            <br>
                        </div>


                        <div class="col-xs-12">
                            <div style="padding-left:0 " class="col-xs-6">
                                <br>
                                <label>Birthday</label>
                                <input type="date" required class="form-control" name="birthday" placeholder="Birthday">
                                <br>
                            </div>
                            <div style="padding-right:0 " class="col-xs-6">
                                <br>
                                <label>Phone Number</label>
                                <input type="text" required class="form-control" name="phone_number"
                                       placeholder="Phone Number">
                                <br>
                            </div>
                            <div style=" ;padding-left:0 " class="col-xs-6">
                                <br>
                                <label>Email</label>
                                <input type="text" required class="form-control" name="email" placeholder="Email">
                                <br>
                            </div>
                            <div style="padding-right:0  " class="col-xs-6">
                                <br>
                                <div class="form-group">
                                    <label>Status</label>
                                    <br>
                                    <select name="status" required class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Shut Down</option>
                                    </select>
                                </div>
                            </div>
                            <div style="padding: 0 !important;" class="col-xs-12">
                                <label>
                                    Describe Product</label>
                                <div style="padding: 0 !important;" class="box-body pad">

                    <textarea required id="editor1" name="editor1" rows="10" cols="80"
                              style="visibility: hidden; display: none;">
                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-right:5px; " class="btn-group">
                        <br>
                        <input type="submit" class="btn btn-primary" name="check" value="Save">

                    </div>

                    <div class="btn-group">
                        <br>
                        <a href="<?php echo globalUrl($cdUpRefArray) ?>home/tour/detail_tour/">
                            <input type="button" onclick="confirm('Discard the change')" class="btn btn-danger"
                                   value="Cancel">
                        </a>
                    </div>

                </form>

            </div>
            <!-- /.box-body -->
        </div>

    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>
