<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;
    include "../../../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php");
    ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tour-Travel
                <small>Create Tour</small>
            </h1>

        </section>

        <div style="margin: 20px;" class="box box-primary">
            <div class="box-header with-border">

            </div>
            <div class="box-body">

                <?php

                editCategory();
                ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-xs-2">
                        <br>
                        <div class="form-group">
                            <label for="imgInp">Choose Image</label>
                            <br>
                            <img width="100px" id="blah"
                                 src="<?php echo globalUrl($cdUpRefArray); ?>../images/tour/category/<?php echo $result['images'];
                                 ?>"
                                 alt="your image"/>

                            <br>
                            <input type="file" onchange="validateFileType();" class="form-control-file"
                                   name="image_cate" id="imgInp">
                        </div>
                        <br>
                    </div>
                    <div class="col-xs-10">
                        <div class="col-xs-12">
                            <br>
                            <label>Name Category</label>
                            <input type="text" required class="form-control" value="<?php echo $result['category'] ?>"
                                   name="category" placeholder="Name Product">
                            <br>
                        </div>
                        <div style="padding: 0" class="col-xs-6">

                            <div class="form-group">
                                <label>Status </label>
                                <br>
                                <select name="status" class="form-control">
                                    <?php if ($result['status'] == 1) { ?>
                                        <option <?php echo "selected" ?> value="1">Active</option>
                                        <option value="0">Shut Down</option>
                                    <?php } else { ?>
                                        <option value="1">Active</option>
                                        <option <?php echo "selected" ?> value="0">Shut Down</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div style="padding: 0" class="col-xs-12">
                            <label>
                                Describe Category</label>
                            <div class="box-body pad">
                                <form>
                    <textarea id="editor1" name="editor1" rows="10" cols="80"
                              style="visibility: hidden; display: none;"><?php echo $result['describe'] ?>
                    </textarea>
                            </div>
                        </div>
                    </div>
                    <div style="margin-right:5px; " class="btn-group">
                        <br>
                        <input type="submit" class="btn btn-primary" name="check" value="Save">

                    </div>

                    <div class="btn-group">
                        <br>
                        <a href="<?php echo globalUrl($cdUpRefArray) ?>home/tour/category/">
                            <input type="button" class="btn btn-danger" value="Cancel">
                        </a>
                    </div>

                </form>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>
