<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;

    include "../../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php");

    ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tour-Travel
                <small>All Order</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo globalUrl($cdUpRefArray); ?>home/"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>



        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Tour-Travel</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <form action="export.php" method="post">
                                            <button class="btn bg-olive margin" name="btnExport" type="submit"> Excel</button>
                                        </form>
                                    </div>
                                    <div class="col-sm-6">

                                        <div id="example1_filter" class="dataTables_filter">
                                            <form action="" method="post">
                                                <label>Search:
                                                    <input type="search" name="search_text" id="search_text"
                                                           placeholder=""
                                                           class="form-control  input-sm"/>
                                                    <input type="submit" name="search"
                                                           class="btn btn-primary" value="Search">
                                            </form>

                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <img src="" alt="">
                                <div class="row">
                                    <div id="result" class="col-sm-12 ">
                                        <?php getAllOrderTour(); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="dataTables_info" id="example1_info" role="status"
                                             aria-live="polite">Showing 1 to 10 of 57 entries
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                            <ul class="pagination">
                                                <li class="paginate_button previous disabled" id="example1_previous"><a
                                                            href="#" aria-controls="example1" data-dt-idx="0"
                                                            tabindex="0">Previous</a>
                                                </li>
                                                <li class="paginate_button active"><a href="#" aria-controls="example1"
                                                                                      data-dt-idx="1" tabindex="0">1</a>
                                                </li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="2" tabindex="0">2</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="3" tabindex="0">3</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="4" tabindex="0">4</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="5" tabindex="0">5</a></li>
                                                <li class="paginate_button "><a href="#" aria-controls="example1"
                                                                                data-dt-idx="6" tabindex="0">6</a></li>
                                                <li class="paginate_button next" id="example1_next"><a href="#"
                                                                                                       aria-controls="example1"
                                                                                                       data-dt-idx="7"
                                                                                                       tabindex="0">Next</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>


        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>

    <!-- Control Sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<!-- ./wrapper -->

<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>