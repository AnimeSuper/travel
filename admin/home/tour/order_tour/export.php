<?php
require "./PHPExcel-1.8/Classes/PHPExcel.php";
require "../../../db/db.php";
if (isset($_POST['btnExport'])) {

    $objExcel = new PHPExcel();
    $objExcel->setActiveSheetIndex(0);
    $sheet = $objExcel->getActiveSheet()->setTitle('order_list');//tạo sheet cho file excel
    $rowCount = 1;
    //Đặt dữ liệu cho cột
    // |A1|B1|C1|..|n1|
    // |A2|B2|C2|..|n1|
    // |..|..|..|..|..|
    // |An|Bn|Cn|..|nn|
    $sheet->setCellValue('A' . $rowCount, 'Tour Name');
    $sheet->setCellValue('B' . $rowCount, 'First Name');
    $sheet->setCellValue('C' . $rowCount, 'Last Name');
    $sheet->setCellValue('D' . $rowCount, 'Email');
    $sheet->setCellValue('E' . $rowCount, 'Phone Number');
    $sheet->setCellValue('F' . $rowCount, 'People');
    //Truy vấn dữ liệu
    $sql = "SELECT tour_list.tour_name, order_list.first_name, order_list.last_name, order_list.email,order_list.phone_number,order_list.people FROM `order_list` 
INNER JOIN tour_list on tour_list.id = order_list.id_tour 
WHERE tour_list.status=1";
    $result = $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $rows) {
        $rowCount++;
        $sheet->setCellValue('A' . $rowCount, $rows['tour_name']);
        $sheet->setCellValue('B' . $rowCount, $rows['first_name']);
        $sheet->setCellValue('C' . $rowCount, $rows['last_name']);
        $sheet->setCellValue('D' . $rowCount, $rows['email']);
        $sheet->setCellValue('E' . $rowCount, $rows['phone_number']);
        $sheet->setCellValue('F' . $rowCount, $rows['people']);
    }

    //Lấy dữ liệu trong db
    $objWrite = new PHPExcel_Writer_Excel2007($objExcel);
    $fileName = 'order.xlsx';
    //Lưu file excel
    $objWrite->save($fileName);
    //Cấu hình hoàn chỉnh cho file excel
    //Trả về file attachment

    header('Content-Disposition: attachment; filename="' . $fileName . '"');
    //Trả về dữ liệu kiểu excel với đuôi là .xlsx
    header('Content-type: application/vnd.ms-excel');
    header('Content-Length' . filesize($fileName));
    header('Content-Transfer-Encoding:binary');
    header('Cache-Control:must-revalidate');
    header('Pragma:no-cache');
    readfile($fileName);
    return;

}
?>

