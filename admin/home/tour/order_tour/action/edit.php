<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;

    include "../../../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php"); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <?php
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $sql = "SELECT order_list.*,tour_list.tour_name FROM `order_list` 
INNER JOIN tour_list on tour_list.id= order_list.id_tour WHERE order_list.id= $id";

            $result1 = $conn->query($sql)->fetch();

        } ?>

        <!-- Main content -->
        <div style="background: #fff; padding: 20px;" class="col-md-12">
            <div class="col-md-10 col-md-offset-1">
                <div class="pro">
                    <div class="title">
                        <div class="caption">
                            <strong> <i class="fa fa-shopping-cart"></i> Order Details </strong></div>
                        <div class="tools">
                            <label class="label label-success"></label>
                        </div>
                    </div>


                    <div class="pro-body">
                        <h2> <?php if ($result1['status'] == 1) {
                                echo "Status:<h2 style='color: blue'>Active</h2>";
                            } elseif ($result1['status'] == 2) {
                                echo "Status:<h2 style='color: green'>Completed</h2>";
                            } elseif ($result1['status'] == 3) {
                                echo "Status:<h2 style='color: green'>Just Book</h2>";
                            } else {
                                echo "Status:<h2 style='color: red'>Rejected</h2>";
                            } ?> </h2>
                        <h1 class="red">Tour:<?php echo $result1['tour_name'] ?></h1>
                        <h4>First-name:<?php echo $result1['first_name'] ?> </h4>
                        <h4>Last-name:<?php echo $result1['last_name'] ?> </h4>
                        <h4>Mobile:<?php echo $result1['phone_number'] ?> </h4><h4>
                            Email:<?php echo $result1['email'] ?> </h4>
                        <h4>Journey date: <?php echo $result1['date_order'] ?></h4>
                        <h4>People: <?php echo $result1['people'] ?></h4>
                        <h4>Ordered on </h4>
                    </div>
                    <div class="row pull-bottom">
                        <div class="col-sm-4">
                            <a href="active.php?id=<?php echo $result1['id'] ?>"
                               class="btn btn-info btn-block">ACTIVE</a>
                        </div>
                        <div class="col-sm-4">
                            <a onclick="return confirm('Complete this order tour?')"
                               href="completed.php?id=<?php echo $result1['id'] ?>" class="btn btn-success btn-block">COMPLETED</a>
                        </div>
                        <div class="col-sm-4">
                            <a href="rejected.php?id=<?php echo $result1['id'] ?>" class="btn btn-danger btn-block">REJECTED</a>
                        </div>
                    </div>
                    <br>
                    <div class="row pull-bottom">

                        <div class="col-sm-2">
                            <a href="<?php echo globalUrl($cdUpRefArray) ?>home/tour/order_tour/"
                               class="btn btn-primary btn-block">Save</a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>

    <!-- Control Sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<!-- ./wrapper -->

<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>