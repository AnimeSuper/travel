<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;
    include "../../../../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php");
    ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tour-Travel
                <small>Create Album</small>
            </h1>

        </section>

        <div style="margin: 20px;" class="box box-primary">
            <div class="box-header with-border">

            </div>
            <?php echo createImagesAlbum(); ?>
            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-xs-2">
                        <br>
                        <div class="form-group">
                            <label for="imgInp">Choose Image</label>
                            <br>
                            <img width="100px" id="blah" src="<?php echo globalUrl($cdUpRefArray) ?>home/noimage.png"
                                 alt="your image"/>
                            <br>
                            <input required type="file" onchange="validateFileType();" class="form-control-file"
                                   name="images" id="imgInp">
                        </div>
                        <br>
                    </div>
                    <div class="col-xs-10">
                        <div class="col-xs-4">
                            <br>
                            <label>Name Images</label>
                            <input type="text" required class="form-control" name="name"
                                   placeholder="Name Images">
                            <br>
                        </div>
                        <div class="col-xs-4">
                            <br>
                            <label>Tour Name</label>
                            <select name="id_tour" readonly required class="form-control">
                                <option value="<?php
                                echo $tours['id'] ?>"><?php echo $tours['tour_name'] ?></option>
                            </select>
                            <br>
                        </div>
                        <div class="col-xs-4">
                            <br>
                            <label>Status</label>
                            <select name="status" required class="form-control">
                                <option value="1">Active</option>
                                <option value="0">Shut Down</option>
                            </select>

                            </select>
                            <br>
                        </div>


                    </div>
                    <div class="col-xs-12">
                        <div style="margin-right:5px; " class="btn-group">
                            <br>
                            <input type="submit" onclick="confirm('Are you sure you want to save images')"
                                   class="btn btn-primary" name="btn-save" value="Save">

                        </div>


                    </div>
                </form>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>
