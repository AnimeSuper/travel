<!DOCTYPE html>
<html>

<head>
    <?php
    global $cdUpRefArray;
    include "../../../../controler/global_url.php";
    include(globalUrl($cdUpRefArray) . "controler/control.php");

    include(globalUrl($cdUpRefArray) . "admin_layout/head.php");
    echo editTour();
    ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


    <?php include(globalUrl($cdUpRefArray) . "admin_layout/header.php") ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/main_sidebar.php") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tour-Travel
                <small>Create Tour</small>
            </h1>

        </section>

        <div style="margin: 20px;" class="box box-primary">
            <div class="box-header with-border">

            </div>

            <div class="box-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-xs-2">
                        <br>
                        <div class="form-group">
                            <label for="imgInp">Choose Image</label>
                            <br>
                            <img width="100px" id="blah"
                                 src="<?php echo globalUrl($cdUpRefArray); ?>../images/tour/detail/<?php echo $result['images'];
                                 ?>"
                                 alt="your image"/>
                            <br>
                            <input type="file" onchange="validateFileType();" class="form-control-file"
                                   name="images" id="imgInp">
                        </div>
                        <br>
                    </div>
                    <div class="col-xs-10">
                        <div class="col-xs-4">
                            <br>
                            <label>Tour Name</label>
                            <input value="<?php echo $result['tour_name'] ?>" type="text" required class="form-control"
                                   name="tour_name"
                                   placeholder="Tour Name">
                            <br>
                        </div>
                        <div class="col-xs-4">
                            <br>
                            <label>Category</label>
                            <select name="id_category" required class="form-control">
                                <?php foreach ($cates as $rows) { ?>
                                    <option
                                        <?php if ($rows['id'] == $result['id_category']) echo "selected" ?>
                                            value="<?php echo $rows['id'] ?>"><?php echo $rows['category'] ?>
                                    </option>
                                <?php } ?>

                            </select>
                            <br>
                        </div>
                        <div class="col-xs-4">
                            <br>
                            <label>Instructor</label>
                            <select name="id_instructor" required class="form-control">
                                <?php foreach ($instructors as $rows) { ?>
                                    <option
                                        <?php if ($rows['id'] == $result['id_instructor']) echo "selected" ?>
                                            value="<?php echo $rows['id'] ?>"><?php echo $rows['fullname'] ?>
                                    </option>
                                <?php } ?>

                            </select>
                            <br>
                        </div>
                        <div class="col-xs-12">
                            <div style="padding-left:0 " class="col-xs-6">
                                <br>
                                <label>Price</label>
                                <input type="text" value="<?php echo $result['tour_cost'] ?>" required
                                       class="form-control" name="tour_cost" placeholder="Price">
                                <br>
                            </div>
                            <div style="padding-right:0 " class="col-xs-6">
                                <br>
                                <label>Price Sale</label>
                                <input value="<?php echo $result['tour_sale'] ?>" type="text" required
                                       class="form-control" name="tour_sale"
                                       placeholder="Price Sale">
                                <br>
                            </div>
                            <div style=" ;padding-left:0 " class="col-xs-6">
                                <br>
                                <label>Duration</label>
                                <input type="text" value="<?php echo $result['duration'] ?>" required
                                       class="form-control" name="duration" placeholder="Duration">
                                <br>
                            </div>
                            <div style="padding-right:0  " class="col-xs-6">
                                <br>
                                <label>Location</label>
                                <input type="text" value="<?php echo $result['location'] ?>" required
                                       class="form-control" name="location" placeholder="Location">
                                <br>
                            </div>
                            <div style="padding-left:0  " class="col-xs-12">
                                <br>
                                <label>Short Describe</label>
                                <input type="text" value="<?php echo $result['short_desc'] ?>" required
                                       class="form-control" name="short_desc"
                                       placeholder="Short Describe">
                                <br>
                            </div>
                            <div style=" ;padding-left:0 " class="col-xs-6">
                                <br>
                                <label>Date Start</label>
                                <input value="<?php echo $result['date_start']?>" type="text" required class="form-control" name="date_start">
                                <br>
                            </div>
                            <div style="padding-right:0  " class="col-xs-6">
                                <br>
                                <label>Date End </label>
                                <input value="<?php echo $result['date_end']?>" type="text" required class="form-control" name="date_end">
                                <br>
                            </div>
                            <div style="padding: 0;" class="col-xs-12">
                                <div style=" padding: 0" class="col-lg-4">
                                    <div class="form-group">
                                        <label>Voting</label>
                                        <br>
                                        <select name="voting" required class="form-control">
                                            <?php
                                            $select = "selected";
                                            if ($result['voting'] == 0) {
                                                ?>
                                                <option <?php echo $select ?> value="0">0 star</option>
                                                <option value="1">1 star</option>
                                                <option value="2">2 stars</option>
                                                <option value="3">3 stars</option>
                                                <option value="4">4 stars</option>
                                                <option value="5">5 stars</option>
                                                <?php
                                            } else if ($result['voting'] == 1) { ?>
                                                <option value="0">0 star</option>
                                                <option <?php echo $select ?> value="1">1 star</option>
                                                <option value="2">2 stars</option>
                                                <option value="3">3 stars</option>
                                                <option value="4">4 stars</option>
                                                <option value="5">5 stars</option>
                                            <?php } else if ($result['voting'] == 2) { ?>
                                                <option value="0">0 star</option>
                                                <option value="1">1 star</option>
                                                <option <?php echo $select ?> value="2">2 stars</option>
                                                <option value="3">3 stars</option>
                                                <option value="4">4 stars</option>
                                                <option value="5">5 stars</option>
                                            <?php } else if ($result['voting'] == 3) { ?>
                                                <option value="0">0 star</option>
                                                <option value="1">1 star</option>
                                                <option value="2">2 stars</option>
                                                <option <?php echo $select ?> value="3">3 stars</option>
                                                <option value="4">4 stars</option>
                                                <option value="5">5 stars</option>
                                                <?php
                                            } else if ($result['voting'] == 4) { ?>
                                                <option value="0">0 star</option>
                                                <option value="1">1 star</option>
                                                <option value="2">2 stars</option>
                                                <option value="3">3 stars</option>
                                                <option <?php echo $select ?> value="4">4 stars</option>
                                                <option value="5">5 stars</option>
                                                <?php
                                            } else { ?>
                                                <option value="0">0 star</option>
                                                <option value="1">1 star</option>
                                                <option value="2">2 stars</option>
                                                <option value="3">3 stars</option>
                                                <option value="4">4 stars</option>
                                                <option <?php echo $select ?> value="5">5 stars</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <br>
                                        <select name="status" required class="form-control">
                                            <?php if ($result['status'] == 1) { ?>
                                                <option <?php echo "selected" ?> value="1">Active</option>
                                                <option value="0">Shut Down</option>
                                            <?php } else {
                                                ?>
                                                <option value="1">Active</option>
                                                <option <?php echo "selected" ?> value="0">Shut Down</option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div style="padding: 0" class="col-xs-4">

                                    <div class="form-group">
                                        <label>Status Special</label>
                                        <br>
                                        <select name="status_news" required class="form-control">
                                            <?php if ($result['status_special'] == 1) { ?>
                                                <option <?php echo "selected" ?> value="1">Special</option>
                                                <option value="0">Not Special</option>
                                            <?php } else {
                                                ?>
                                                <option value="1">Special</option>
                                                <option <?php echo "selected" ?> value="0">Not Special</option>
                                                <?php
                                            } ?>

                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div style="padding: 0 !important;" class="col-xs-12">
                                <label>
                                    Describe Tour</label>
                                <div style="padding: 0 !important;" class="box-body pad">

                    <textarea required id="editor1" name="editor1" rows="10" cols="80"
                              style="visibility: hidden; display: none;"><?php echo $result['describe'] ?>
                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-right:5px; " class="btn-group">
                        <br>
                        <input type="submit" onclick="confirm('Are you sure you want to save tour')"
                               class="btn btn-primary" name="check" value="Save">

                    </div>

                    <div class="btn-group">
                        <br>
                        <a href="<?php echo globalUrl($cdUpRefArray) ?>home/tour/detail_tour/">
                            <input type="button" onclick=" return confirm('Discard the change')" class="btn btn-danger"
                                   value="Cancel">
                        </a>
                    </div>

                </form>
            </div>
            <!-- /.box-body -->
        </div>

    </div>
    <!-- /.content-wrapper -->
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/footer.php") ?>
    <?php include(globalUrl($cdUpRefArray) . "admin_layout/control-slidebar.php") ?>
</div>
<?php include(globalUrl($cdUpRefArray) . "admin_layout/js.php") ?>
</body>

</html>
