<?php //454521 URL SET_UP, the important stuff for linking
//=============IMPORTANT variable $ROOT_DIRECTORY must be where the project is housed (where the homepage is). ==========================
session_start();



global $cdUpRefArray;
function globalUrl($cdUpRefArray)
{
    global $cdUpRefArray;
    $ROOT_DIRECTORY = "travel";
//PHẢI THAY ĐỔI NÀY HOẶC DỰ ÁN ENTIRE KHÔNG LÀM VIỆC! Mặc định là "php-magic-links"
//    // vì đó là tên của git repo, nhưng bạn có thể đổi tên thư mục gốc và
//    // mọi thứ khác sẽ hoạt động miễn là biến này khớp với tên mới

//======Mã ma thuật để hiển thị lỗi PHP thay vì chỉ đơn giản là một trang trống========\\
//error_reporting(E_ALL);           //longer version = 2 lines
//ini_set('display_errors', '1');
    ini_set('error_reporting', E_ALL);

//Đặt giá trị của tùy chọn cấu hình đã cho. Tùy chọn cấu hình sẽ giữ giá trị mới này trong
// quá trình thực thi tập lệnh và sẽ được khôi phục ở cuối tập lệnh.


// %^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
// PATH SETUP, (đảm bảo nó sử dụng https)
    $domain = "http://";     //bình luận 5 dòng tiếp theo không hoạt động
    if (isset($_SERVER['HTTPS'])) {   //Cách cũ, không được sử dụng
        if ($_SERVER['HTTPS']) {
            $domain = "https://";
        }
    }

    $server = htmlentities($_SERVER['SERVER_NAME'], ENT_QUOTES, "UTF-8");//Hàm htmlentities () chuyển đổi các ký tự thành các thực thể HTML.

    $domain .= $server;//nối máy chủ với năng suất tên miền "http://[your_domain_here]" or "https://[your_domain_here]"

    $phpSelf = htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8");     // Mang lại chuỗi của url SAU tên miền (vì vậy chỉ cần các thư mục và tệp chính xác). Sử dụng htmlentites để xóa bất kỳ giá trị đáng ngờ nào mà ai đó có thể cố gắng vượt qua. Htmlentites giúp tránh các vấn đề bảo mật. // ## $ _SERVER ['PHP_SELF'] trả về đường dẫn url đầy đủ và phần mở rộng tệp, htmlentities () chỉ chuyển đổi các ký tự đặc biệt

    $path_parts = pathinfo($phpSelf);

// Hàm pathinfo () trả về thông tin về đường dẫn tệp.
//có được một mảng kết hợp của url với dirname, tên cơ sở, phần mở rộng và tên tệp


    $split_url = explode('/', $path_parts['dirname']);  //phân chia chuỗi thư mục tại mỗi / ký tự

    $baseLevelIndex = 0;        //used to find the "base directory" in the url. If the site's home is in "topLevel/level1/level2/ROOT_SITE_FOLDER_HERE" then it's 3 folders down, so everything should relate the the url array from index 3. We iterate through the URL array to find the $ROOT_FOLDER, then adjust and make a new array
    for ($i = 0; $i < count($split_url); $i++) {     //loop through the URL
        if ($split_url[$i] == $ROOT_DIRECTORY) {     //SUPER IMPORTANT ($ROOT_DIRECTORY must match the BASE folder that the site lives inside)
            $baseLevelIndex = $i;

            break;    //This stops when the 1st occurence of $ROOT_DIRECTORY is found. COMMENT OUT OR REMOVE THIS  break;  if your actual root directory has a parent folder with the exat same name ()
        }
    }
    $folderCountRaw = count($split_url); //this gives an int of how many folders are in the URL

    $folderCount = $folderCountRaw - $baseLevelIndex -1; //subtract $baseLevelIndex to get the base directory (no matter how deep the file structure, this resets it to a base folder. Then subtract 1 to make the "home" directory be 0 folders up from anything
//0 means the homepage, 1 means top level pages (file is located in 1 folder below $ROOT_DIRECTORY), 2 means 2 levels down, etc.

    $split_url_adjusted = $split_url;
//array to hold the URL parts AFTER the $ROOT_DIRECTORY (remove any directories ABOVE $ROOT_DIRECTORY)
    for ($i = 0; $i < ($folderCountRaw - $folderCount - 1); $i++) {   //remove the beginning indices of the array (anything before $ROOT_DIRETORY)
        unset($split_url_adjusted[$i]);
        //actually remove the element, but the indices will be messed up
    }
    $split_url_adjusted = array_values($split_url_adjusted);     //array_values re-indexes the array. Now this contains a list folderis in the the URL including & AFTER the $ROOT_DIRECTORY

    $containing_folder = $split_url_adjusted[count($split_url_adjusted) - 1];
//IMPORTANT this gets the very last folder in the $split_url_adjusted array (the very last index of an array is 1 less than its size, hence: count($split_url_adjusted) -1 ). This folder "contains" the current page file. Used almost everywhere to tell what page I'm on since all my pages are called 'index.php' but have unique cotaining-folder names
    if ($folderCount == 0) {      //special case for the homepage. Since its actual containing folder is the contents of $ROOT_DIRECTORY, it must be overridden to equal "index". This is to avoid confusion if $ROOT_DIRECTOY is NOT a a good name for the site. This disregards where the site is located & just make the homepage's containing folder = "index". ALSO USED TO PRINT ID'S IN THE BODY TAG FOR EACH PAGE
        $containing_folder = 'index';
    }
    $fileName = $path_parts['filename'];
//not used much, but just in case
    $dirName = $path_parts['dirname'];
//the url of folders (excluding filename). Not used much

    $cdUpRefArray = '';              ////khởi tạo chuỗi rỗng (giả sử nó ở cấp thư mục cao nhất)
    for ($i = 1; $i < $folderCount; $i++) {
        $cdUpRefArray .= '../';      //append ../ for how many levels the currrent folder is below the root
    }

    return $cdUpRefArray;
}

