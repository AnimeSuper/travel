<?php

global $cdUpRefArray;
include(globalUrl($cdUpRefArray) . "db/db.php");
global $conn;
global $output;


//Login
function login()
{
    global $conn,
           $users,
           $cdUpRefArray;
    if (isset($_POST['log'])) {
        $username = $_POST['username'];
        $user_sql = "SELECT * FROM `users` where username='$username' and role =0";
        $users = $conn->query($user_sql)->fetch();
        $password = md5($_POST['password']);
        if ($users['role'] == 0) {

            if (!$username || !$password) {
                echo "Please enter your full username and password. <a href='index.php'>Return</a>";
                exit;
            }
            if ($username != $users['username']) {
                echo "This username does not exist. Please check again. <a href='index.php'>Return</a>";
                exit;
            }

            if ($password != $users['password']) {
                echo "Incorrect password. Please enter again. <a href='index.php'> Return</a>";
                exit;
            }
            $_SESSION['username'] = $username;

            header("Location:" . globalUrl($cdUpRefArray) . "home/");

        }
    }
    ?>
    <form action="index.php?do=login" method="post">
        <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="Username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8"></div>
            <!-- /.col -->
            <div class="col-xs-4">

                <button type="submit" name="log" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
    <?php
}

//Category
function getAllCategory()
{
    global $conn;
    $category_sql = "SELECT * FROM `category` order by id desc ";
    $categorys = $conn->query($category_sql)->fetchAll();
    ?>
    <table style="" id="example1"
           class="table table-bordered table-striped dataTable"
           role="grid" aria-describedby="example1_info">
        <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="example1"
                rowspan="1" colspan="1" aria-sort="ascending" style="width: 297px;">
                #
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 322px;">Category
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Images
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Update at
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="text-align: center;">Status
            </th>

            <th class="sorting" style="text-align: center;" tabindex="0"
                aria-controls="example1" rowspan="1"
                colspan="2" style="">Action
            </th>
        </tr>
        </thead>
        <tbody>
        <?php global $cdUpRefArray;
        foreach ($categorys as $key => $rows) { ?>

            <tr role="row" class="<?php if ($key % 1) echo "odd";
            else echo "even"; ?>">
                <td class="sorting_1"><?php echo($key + 1); ?></td>
                <td><?php echo $rows['category'] ?>
                    <br>
                    Id: <?php echo $rows['id'] ?>
                </td>

                <td align="center"><img width="100px"
                                        src="<?php echo globalUrl($cdUpRefArray) ?>../../travel/images/tour/category/<?php echo $rows['images'] ?>"
                                        alt=""></td>
                <td><?php

                    $date = date_create($rows['update_at']);
                    echo date_format($date, "d/m/Y") ?>

                </td>
                <td align="center">
                    <span class="label  label-<?php if ($rows['status'] == 1) {
                        echo "primary";
                    } else {
                        echo "danger";
                    } ?>"><?php if ($rows['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Shut Down";
                        } ?></span>
                </td>

                <td style="text-align: center;">
                    <div class="btn-group ">
                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/category/action/edit.php?id=<?php echo $rows['id'] ?>">
                            <button type="submit" class="btn btn-info">Edit</button>
                        </a>
                    </div>
                </td>
                <td style="text-align: center;">
                    <div class="btn-group">
                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/category/action/delete.php?id=<?php echo $rows['id'] ?>">
                            <button onclick="return confirm('Delete Category!')"
                                    type="submit" class="btn btn-danger">Delete
                            </button>
                        </a>
                    </div>
                </td>
            </tr>

        <?php }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th rowspan="1" colspan="1">#</th>
            <th rowspan="1" colspan="1">Category</th>
            <th rowspan="1" colspan="1"> Images</th>
            <th rowspan="1" colspan="1"> Update at</th>
            <th style="text-align: center;" rowspan="1" colspan="1">Status</th>
            <th style="text-align: center;" rowspan="1" colspan="2"> Action</th>
        </tr>
        </tfoot>
    </table>
    <?php

}

//Add new category
function createCategory()
{
    global $conn;
    global $cdUpRefArray;

    if (isset($_POST['check'])) {
        $cate = $_POST['category'];
        $date = date("Y/m/d/H/i/s");

        $image_cate = $_FILES['image_cate']['name'];
        $tmpA_cate = $_FILES['image_cate']['tmp_name'];
        move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/tour/category/" . $image_cate);

        $describe = $_POST['editor1'];
        $status = $_POST['status'];
        $sql = "INSERT INTO `category` VALUES (NULL ,'$cate','$image_cate','$describe','$date','$date',$status)";

        $kq = $conn->exec($sql);
        if ($kq == 1) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/category/");
            ob_end_flush();
        } else {
            echo "
    <script>alert('Lỗi')</script>";
        }

    }
}

function getIdCate()
{

    global $conn;
    try {

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $sql = "SELECT * FROM `category` WHERE id= $id";
            $result = $conn->query($sql)->fetch();
            return $result;
        }
    } catch (\mysql_xdevapi\Exception $e) {
        return $e->getMessage();
    }

}

//Edit category
function editCategory()
{
    global $conn;
    global $cdUpRefArray;
    global $result;
    $result = getIdCate();
    $id = $result['id'];
    if (isset($_POST['check'])) {
        $cate = $_POST['category'];
        $date = date("Y/m/d");
        $image_cate = $_FILES['image_cate']['name'];
        if (isset($image_cate) && !empty($image_cate)) {
            $tmpA_cate = $_FILES['image_cate']['tmp_name'];
            move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/tour/category/" . $image_cate);
        } else {
            $image_cate = $result['images'];
        }

        $describe = $_POST['editor1'];
        $status = $_POST['status'];
        $sql = "UPDATE `category` SET `category`='$cate',`images`='$image_cate',`describe`='$describe',`update_at`='$date',`status`=$status WHERE id= $id";

        $kq = $conn->exec($sql);
        if ($kq == 1) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/category/");
            ob_end_flush();
        } else {
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/category/");

        }

    }
}

//Delete Category
function deleteCategory()
{
    global $conn;
    global $cdUpRefArray;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $sql = "DELETE FROM `category` WHERE id=$id";

        $kq = $conn->prepare($sql);
        if ($kq->execute()) {

            echo "<script>alert('Delete Successful')</script>";
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/category/");
        } else {
            echo "<script>alert('Delete tour before delete category')</script>";

        }
    }
}

//Tour_list
function getAllTour()
{
    global $conn;

    $tour_sql = "SELECT tour_list.*,category.category ,tour_instructor.fullname FROM `tour_list` 
INNER JOIN category on category.id = tour_list.id_category 
INNER JOIN tour_instructor on tour_instructor.id = tour_list.id_instructor 
";

    $tours = $conn->query($tour_sql)->fetchAll();

    ?>
    <table style="" id="example1"
           class="table table-bordered table-striped dataTable"
           role="grid" aria-describedby="example1_info">
        <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="example1"
                rowspan="1" colspan="1" aria-sort="ascending" style="width: 297px;">
                #
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 322px;">Category
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Tour Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Images
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Location
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Update_at
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Viewer
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Voting
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Instructor
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style=" text-align:center;width: 257px;">Status
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="text-align:center; width: 257px;">Status Special
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Album
            </th>
            <th class="sorting" style="text-align: center;" tabindex="0"
                aria-controls="example1" rowspan="1"
                colspan="2" style="">Action
            </th>
        </tr>
        </thead>
        <tbody>
        <?php global $cdUpRefArray;
        foreach ($tours as $key => $rows) {
            ?>
            <tr role="row" class="<?php if ($key % 1) echo "odd";
            else echo "even"; ?>">
                <td class="sorting_1"><?php echo($key + 1); ?></td>
                <td><?php echo $rows['category'] ?>
                    <br>
                    Id: <?php echo $rows['id_category'] ?>
                </td>
                <td><?php echo $rows['tour_name'] ?>
                    <br>
                    Id: <?php echo $rows['id'] ?>
                </td>
                <td align="center"><img width="100px"
                                        src="<?php echo globalUrl($cdUpRefArray) ?>../../travel/images/tour/detail/<?php echo $rows['images'] ?>"
                                        alt=""></td>
                <td><?php echo $rows['location'] ?>


                </td>
                <td><?php
                    $date = date_create($rows['update_at']);
                    echo date_format($date, "d-m-Y") ?>
                <td><?php echo $rows['viewer'] ?>
                <td>
                    <?php if ($rows['voting'] == 0) { ?>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                    <?php } else if ($rows['voting'] == 1) { ?>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <?php
                    } else if ($rows['voting'] == 2) { ?>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                    <?php } else if ($rows['voting'] == 3) { ?>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                    <?php } else if ($rows['voting'] == 4) { ?>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star-o"></i>
                    <?php } else if ($rows['voting'] == 5) { ?>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>
                        <i class="fa fa-fw fa-star"></i>

                    <?php } ?>
                </td>

                <td><?php echo $rows['fullname'] ?>
                <td align="center">
                    <span class="label  label-<?php if ($rows['status'] == 1) {
                        echo "primary";
                    } else {
                        echo "danger";
                    } ?>"><?php if ($rows['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Shut Down";
                        } ?></span>
                </td>
                <td align="center">
                    <span class="label  label-<?php if ($rows['status_special'] == 1) {
                        echo "primary";
                    } else {
                        echo "danger";
                    } ?>"><?php if ($rows['status_special'] == 1) {
                            echo "Special";
                        } else {
                            echo "Not Special";
                        } ?></span>
                </td>
                <td align="center">
                    <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/detail_tour/album/?id=<?php echo $rows['id'] ?>">
                        <button type="submit" class="btn btn-info"> Show Album</button>
                    </a>

                </td>
                <td style="text-align: center;">
                    <div class="btn-group ">
                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/detail_tour/action/edit.php?id=<?php echo $rows['id'] ?>">
                            <button type="submit" class="btn btn-info">Edit</button>
                        </a>
                    </div>
                </td>
                <td style="text-align: center;">
                    <div class="btn-group">
                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/detail_tour/action/delete.php?id=<?php echo $rows['id'] ?>">
                            <button onclick="return confirm('Delete Product!')"
                                    type="submit" class="btn btn-danger">Delete
                            </button>
                        </a>
                    </div>
                </td>
            </tr>
            <?php
        }

        ?>
        <tfoot>
        <tr>
            <th rowspan="1" colspan="1">#</th>
            <th rowspan="1" colspan="1">Category</th>
            <th rowspan="1" colspan="1"> Tour Name</th>
            <th rowspan="1" colspan="1">Images</th>
            <th rowspan="1" colspan="1">Location</th>
            <th rowspan="1" colspan="1"> Update_at</th>
            <th rowspan="1" colspan="1"> Viewer</th>
            <th rowspan="1" colspan="1"> Voting</th>

            <th rowspan="1" colspan="1"> Instructor</th>
            <th style="text-align: center;" rowspan="1" colspan="1">Status</th>
            <th style="text-align: center;" rowspan="1" colspan="1">Status Special</th>
            <th rowspan="1" colspan="1">Album</th>
            <th style="text-align: center;" rowspan="1" colspan="2"> Action</th>
        </tr>
        </tfoot>
    </table>
    <?php
}

//Add new tour
function createTour()
{
    global $conn;
    global $cdUpRefArray;
    global $cates;
    global $instructors;
    $cate_sql = "SELECT * FROM `category` ";
    $cates = $conn->query($cate_sql)->fetchAll();
    $instructor_sql = "SELECT * FROM `tour_instructor`";
    $instructors = $conn->query($instructor_sql)->fetchAll();
    if (isset($_POST['check'])) {
        $id_category = $_POST['id_category'];
        $id_instructor = $_POST['id_instructor'];
        $tour_name = $_POST['tour_name'];
        $tour_cost = $_POST['tour_cost'];
        $tour_sale = $_POST['tour_sale'];
        $duration = $_POST['duration'];
        $location = $_POST['location'];
        $images = $_FILES['images']['name'];
        $tmpA_cate = $_FILES['images']['tmp_name'];
        move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/tour/detail/" . $images);
        $short_desc = $_POST['short_desc'];
        $describe = $_POST['editor1'];
        $date = date("Y/m/d");
        $date_start = $_POST['date_start'];
        $date_end = $_POST['date_end'];
        $voting = $_POST['voting'];
        $status = $_POST['status'];
        $status_special = $_POST['status_news'];
        $sql = "INSERT INTO `tour_list`
VALUES (NULL ,$id_category,$id_instructor,'$tour_name','$tour_cost','$tour_sale','$duration','$location','$images','$short_desc','$describe','$date','$date','',$voting,'$date_start','$date_end',$status,$status_special)";

        $kq = $conn->exec($sql);
        if ($kq == 1) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/detail_tour/");
            ob_end_flush();
        } else {
            echo "
    <script>alert('Lỗi')</script>";
        }

    }
}

//Get ID Tour
function getIdTour()
{

    global $conn;
    try {

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $sql = "SELECT * FROM `tour_list` WHERE id= $id";
            $result = $conn->query($sql)->fetch();
            return $result;
        }
    } catch (PDOException $e) {
        return $e->getMessage();
    }

}

//Edit tour
function editTour()
{
    global $conn,
           $cdUpRefArray,
           $result,
           $cates,
           $instructors;
    $cate_sql = "SELECT * FROM `category` ";
    $cates = $conn->query($cate_sql)->fetchAll();
    $instructor_sql = "SELECT * FROM `tour_instructor`";
    $instructors = $conn->query($instructor_sql)->fetchAll();
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $sql = "SELECT * FROM `tour_list` WHERE id= $id";
        $result = $conn->query($sql)->fetch();
    } else {
        echo "Không tìm thấy tour";
        exit;
    }
    if (isset($_POST['check'])) {
        $id_category = $_POST['id_category'];
        $id_instructor = $_POST['id_instructor'];
        $tour_name = $_POST['tour_name'];
        $tour_cost = $_POST['tour_cost'];
        $tour_sale = $_POST['tour_sale'];
        $duration = $_POST['duration'];
        $location = $_POST['location'];
        $images = $_FILES['images']['name'];
        if (isset($images) && !empty($images)) {
            $tmpA_cate = $_FILES['images']['tmp_name'];
            move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/tour/detail/" . $images);
        } else {
            $images = $result['images'];
        }
        $short_desc = $_POST['short_desc'];
        $describe = $_POST['editor1'];
        $date = date("Y/m/d");
        $voting = $_POST['voting'];
        $date_start = $_POST['date_start'];
        $date_end = $_POST['date_end'];
        $status = $_POST['status'];
        $status_special = $_POST['status_news'];
        $sql = "UPDATE `tour_list` 
SET `id_category`= $id_category,id_instructor='$id_instructor',
`tour_name`='$tour_name',`tour_cost`='$tour_cost',
`tour_sale`='$tour_sale',`duration`='$duration',
`location`='$location',`images`='$images',
`short_desc`='$short_desc',`describe`='$describe',
`update_at`='$date',
`voting`=$voting,date_start='$date_start',
date_end='$date_end',`status`=$status,
`status_special`=$status_special WHERE id= $id";

        $kq = $conn->exec($sql);
        if ($kq == 1) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/detail_tour/");
            ob_end_flush();
        } else {
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/detail_tour/");
        }

    }
}

//Delete Category
function deletetour()
{
    global $conn;
    global $cdUpRefArray;
    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $sql = "DELETE FROM `tour_list` WHERE id=$id";

        $kq = $conn->prepare($sql);
        if ($kq->execute()) {
            echo "<script>alert('Delete Successful')</script>";
            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/detail_tour/");
        } else {
            echo "<script>alert('DELETE ALBUM BEFORE DELETE TOUR')</script>";
            exit;
        }
    }
}

//Album tour
function getAllAlbum()
{
    global $conn;
    global $cdUpRefArray;
    $result = getIdTour();
    $id = $result['id'];
    $album_sql = "SELECT * from album_list WHERE id_tour=$id order  by id desc ";

    $album = $conn->query($album_sql)->fetchAll();

    foreach ($album as $key => $rows) {
        ?>
        <tr role="row" class="<?php if ($key % 1) echo "odd";
        else echo "even"; ?>">
            <td class="sorting_1"><?php echo($key + 1); ?></td>

            <td><?php echo $rows['name'] ?>
                <br>
                Id: <?php echo $rows['id'] ?>
            </td>
            <td align="center"><img width="200px"
                                    src="<?php echo globalUrl($cdUpRefArray) ?>../../travel/images/tour/detail/album/<?php echo $rows['images'] ?>"
                                    alt=""></td>


            </td>
            <td><?php
                $date = date_create($rows['update_at']);
                echo date_format($date, "d-m-Y") ?>


            <td align="center">
                    <span class="label  label-<?php if ($rows['status'] == 1) {
                        echo "primary";
                    } else {
                        echo "danger";
                    } ?>"><?php if ($rows['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Shut Down";
                        } ?></span>
            </td>


            <!--            <td style="text-align: center;">-->
            <!--                <div class="btn-group ">-->
            <!--                    <a href="-->
            <?php //echo globalUrl($cdUpRefArray); ?><!--home/tour/detail_tour/album/action/edit.php?id=-->
            <?php //echo $rows['id'] ?><!--">-->
            <!--                        <button type="submit" class="btn btn-info">Edit</button>-->
            <!--                    </a>-->
            <!--                </div>-->
            <!--            </td>-->
            <td style="text-align: center;">
                <div class="btn-group">
                    <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/detail_tour/album/action/delete.php?id=<?php echo $rows['id'] ?>&tour=<?php echo $rows['id_tour'] ?>">
                        <button onclick="return confirm('Delete Product!')"
                                type="submit" class="btn btn-danger">Delete
                        </button>
                    </a>
                </div>
            </td>
        </tr>
        <?php

    }


}

//Create Album Images
function createImagesAlbum()
{
    global $conn;
    global $cdUpRefArray;
    global $tours;
    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];
        $tour_sql = "SELECT  tour_list.* FROM `tour_list`
   WHERE id=$id ";

        $tours = $conn->query($tour_sql)->fetch();
    }
    if (isset($_POST['btn-save'])) {
        $id_tour = $_POST['id_tour'];
        $name = $_POST['name'];
        $images = $_FILES['images']['name'];
        $tmpA_cate = $_FILES['images']['tmp_name'];
        move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/tour/detail/album/" . $images);
        $date = date("Y/m/d");
        $status = $_POST['status'];

        $sql = "INSERT INTO `album_list` VALUES (NULL ,$id_tour,'$name','$images','$date','$date',$status)";

        $result = $conn->exec($sql);
        if ($result == 1) {
            echo "<h4 style='background: #9cf39c'>Data success! Return to the album page => " . "<a href='../index.php?id=$id'>Home</a></h4>";
        } else {
            echo "Loi";
        }
    }
}

//Delete Images Album
function deleteImagesAlbum()
{
    global $conn;
    global $cdUpRefArray;


    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];
        $id_tour = $_GET['tour'];
        $sql = "DELETE FROM `album_list` WHERE id=$id";

        $result = $conn->prepare($sql);
        if ($result->execute()) {

            header("Location:" . globalUrl($cdUpRefArray) . "home/tour/detail_tour/album/?id=$id_tour");

        } else {
            echo "<script>alert('Delete False')</script>";
            exit;
        }
    }
}

// Get all Instructor
function getAllIntructor()
{
    global $conn;

    $intrucstor_sql = "SELECT * FROM `tour_instructor` order by id desc ";
    $intrucstor = $conn->query($intrucstor_sql)->fetchAll();


    ?>
    <table style="" id="example1"
           class="table table-bordered table-striped dataTable"
           role="grid" aria-describedby="example1_info">
        <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="example1"
                rowspan="1" colspan="1" aria-sort="ascending" style="width: 297px;">
                #
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 322px;">Full Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Birthday
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Images
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Email
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Phone Number
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="text-align: center;">Status
            </th>

            <th class="sorting" style="text-align: center;" tabindex="0"
                aria-controls="example1" rowspan="1"
                colspan="2" style="">Action
            </th>
        </tr>
        </thead>
        <tbody>
        <?php global $cdUpRefArray;
        foreach ($intrucstor as $key => $rows) { ?>

            <tr role="row" class="<?php if ($key % 1) echo "odd";
            else echo "even"; ?>">
                <td class="sorting_1"><?php echo($key + 1); ?></td>
                <td><?php echo $rows['fullname'] ?>
                    <br>
                    Id: <?php echo $rows['id'] ?>
                </td>
                <td><?php

                    $date = date_create($rows['birthday']);
                    echo date_format($date, "d/m/Y") ?>

                </td>

                </td>
                <td align="center"><img width="100px"
                                        src="<?php echo globalUrl($cdUpRefArray) ?>../../travel/images/users/guilder/<?php echo $rows['images'] ?>"
                                        alt=""></td>
                <td> <?php echo $rows['email'] ?></td>
                <td> <?php echo $rows['phone_number'] ?></td>

                <td align="center">
                    <span class="label  label-<?php if ($rows['status'] == 1) {
                        echo "primary";
                    } else {
                        echo "danger";
                    } ?>"><?php if ($rows['status'] == 1) {
                            echo "Active";
                        } else {
                            echo "Shut Down";
                        } ?></span>
                </td>

                <td style="text-align: center;">
                    <div class="btn-group ">
                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/user/guilder/action/edit.php?id=<?php echo $rows['id'] ?>">
                            <button type="submit" class="btn btn-info">Edit</button>
                        </a>
                    </div>
                </td>
                <td style="text-align: center;">
                    <div class="btn-group">
                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/user/guilder/action/delete.php?id=<?php echo $rows['id'] ?>">
                            <button onclick="return confirm('Delete Instructor!')"
                                    type="submit" class="btn btn-danger">Delete
                            </button>
                        </a>
                    </div>
                </td>
            </tr>

        <?php }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th rowspan="1" colspan="1">#</th>
            <th rowspan="1" colspan="1">Full Name</th>
            <th rowspan="1" colspan="1">Birthday</th>
            <th rowspan="1" colspan="1">Images</th>
            <th rowspan="1" colspan="1">Email</th>
            <th rowspan="1" colspan="1">Phone Number</th>
            <th style="text-align: center;" rowspan="1" colspan="1">Status</th>
            <th style="text-align: center;" rowspan="1" colspan="2"> Action</th>
        </tr>
        </tfoot>
    </table>
    <?php

}

//Create Instructor
function createInstructor()
{
    global $conn;
    global $cdUpRefArray;
    global $tours;
    $tour_sql = "SELECT  tour_list.* FROM `tour_list`";
    $tours = $conn->query($tour_sql)->fetchAll();
    if (isset($_POST['check'])) {
        $full_name = $_POST['fullname'];

        $images = $_FILES['images']['name'];
        $tmpA_cate = $_FILES['images']['tmp_name'];
        move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/users/guilder/" . $images);
        $birthday = $_POST['birthday'];
        $phone_number = $_POST['phone_number'];
        $email = $_POST['email'];
        $describe = $_POST['editor1'];
        $status = $_POST['status'];
        $sql = "INSERT INTO `tour_instructor` VALUES (NULL ,'$full_name','$birthday','$images','$email','$phone_number','$describe',$status)";

        $result = $conn->exec($sql);
        if ($result == 1) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/user/guilder/");
        } else {
            echo "Loi";
        }
    }

}

//Get ID Instructor
function getIdInstructor()
{
    global $conn;
    try {

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $sql = "SELECT * FROM `tour_instructor` WHERE id= $id";
            $result = $conn->query($sql)->fetch();
            return $result;
        }
    } catch (\mysql_xdevapi\Exception $e) {
        return $e->getMessage();
    }
}

//Edit Instructor
function editInstructor()
{
    global $conn;
    global $cdUpRefArray;
    global $tours;
    $result = getIdInstructor();

    $id_instructor = $result['id'];
    $tour_sql = "SELECT  tour_list.* FROM `tour_list`";
    $tours = $conn->query($tour_sql)->fetchAll();
    if (isset($_POST['check'])) {
        $full_name = $_POST['fullname'];


        $images = $_FILES['images']['name'];
        if (isset($images) && !empty($images)) {
            $images = $_FILES['images']['name'];
            $tmpA_cate = $_FILES['images']['tmp_name'];
            move_uploaded_file($tmpA_cate, "" . globalUrl($cdUpRefArray) . "../../travel/images/users/guilder/" . $images);
        } else {
            $images = $result['images'];
        }
        $birthday = $_POST['birthday'];
        $phone_number = $_POST['phone_number'];
        $email = $_POST['email'];
        $describe = $_POST['editor1'];
        $status = $_POST['status'];
        $sql = "UPDATE `tour_instructor` SET
`fullname`='$full_name',
 `birthday`='$birthday',`images`='$images',
 `email`='$email',`phone_number`='$phone_number',
`describe`='$describe',
 `status`=$status WHERE id= $id_instructor";

        $kq = $conn->exec($sql);
        if ($kq == 1) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/user/guilder/");
        } else {
            echo "Loi";
        }
    }
}

//Delete Instructor
function deleteInstructor()
{
    global $conn;
    global $cdUpRefArray;


    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];

        $sql = "DELETE FROM `tour_instructor` WHERE id=$id";

        $result = $conn->prepare($sql);
        if ($result->execute()) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/user/guilder/");
        } else {
            echo "<script>alert('Delete False')</script>";
            exit;
        }
    }

}

//Get all order tour
function getAllOrderTour()
{
    global $conn;
    if (isset($_POST['search'])) {
        $search = trim($_POST['search_text']);

        $order_sql = "SELECT order_list .*, tour_list . tour_name ,tour_instructor . fullname FROM `order_list` 
INNER JOIN tour_list on tour_list . id = order_list . id_tour 
INNER JOIN tour_instructor on tour_instructor . id = order_list . id_instructor 
WHERE (tour_list.tour_name like '%$search%') 
OR (order_list.first_name like '%$search%')
 OR (order_list.last_name like '%$search%')
 OR (order_list.email like '%$search%')
  OR (order_list.phone_number like '%$search%')
   OR (tour_instructor . fullname like '%$search%')
   ";

    } else {
        $order_sql = "SELECT order_list .*, tour_list . tour_name ,tour_instructor . fullname FROM `order_list` 
INNER JOIN tour_list on tour_list . id = order_list . id_tour 
INNER JOIN tour_instructor on tour_instructor . id = order_list . id_instructor 
order by order_list . id desc";
    }
    $orders = $conn->query($order_sql)->fetchAll();

    ?>
    <table style="" id="example1"
           class="table table - bordered table - striped dataTable"
           role="grid" aria-describedby="example1_info">
        <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="example1"
                rowspan="1" colspan="1" aria-sort="ascending" style="width: 297px;">
                #
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 322px;">Tour
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">First Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Last Name
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Email
            </th>

            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Phone Number
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Date Order
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">People
            </th>
            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style="width: 257px;">Instructor
            </th>


            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1"
                colspan="1" style=" text - align:center;width: 257px;">Status
            </th>
            <th class="sorting" style="text - align: center;" tabindex="0"
                aria-controls="example1" rowspan="1"
                colspan="2" style="">Action
            </th>
        </tr>
        </thead>
        <tbody>
        <?php global $cdUpRefArray;
        foreach ($orders as $key => $rows) {
            ?>
            <tr role="row" class=" <?php if ($key % 1) echo "odd";
            else echo "even"; ?>">
                <td class="sorting_1"><?php echo($key + 1); ?></td>
                <td><?php echo $rows['tour_name'] ?>
                    <br>
                    Id: <?php echo $rows['id_tour'] ?>
                </td>
                <td><?php echo $rows['first_name'] ?>

                </td>

                <td><?php echo $rows['last_name'] ?>

                <td><?php echo $rows['email'] ?>
                <td><?php echo $rows['phone_number'] ?>
                </td>
                <td><?php
                    $date = date_create($rows['date_order']);
                    echo date_format($date, "d-m-Y") ?>
                <td><?php echo $rows['people'] ?>

                <td><?php echo $rows['fullname'] ?>

                <td align="center">
                    <span class="label  label-<?php if ($rows['status'] == 1) {
                        echo "primary";
                    } else if ($rows['status'] == 2) {
                        echo "success";
                    } else if ($rows['status'] == 3) {
                        echo "success";
                    } else {
                        echo "danger";
                    }


                    ?>">
                        <?php if ($rows['status'] == 1) {
                            echo "Active";
                        } else if ($rows['status'] == 2) {
                            echo "Completed";
                        } else if ($rows['status'] == 3) {
                            echo "Just Book";
                        } else {
                            echo "Rejected";
                        }
                        ?></span>
                </td>


                <td style="text-align: center;">
                    <div class="btn-group ">

                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/order_tour/action/edit.php?id=<?php echo $rows['id'] ?>">
                            <?php if ($rows['status'] == 2) { ?>
                                <button disabled type="submit" class="btn btn-info">Edit</button>
                                <?php
                            } else { ?>
                                <button type="submit" class="btn btn-info">Edit</button>
                            <?php } ?>
                        </a>
                    </div>
                </td>
                <td style="text-align: center;">
                    <div class="btn-group">
                        <a href="<?php echo globalUrl($cdUpRefArray); ?>home/tour/order_tour/action/delete.php?id=<?php echo $rows['id'] ?>">
                            <button onclick="return confirm('Delete order!')"
                                    type="submit" class="btn btn-danger">Delete
                            </button>
                        </a>
                    </div>
                </td>
            </tr>
            <?php
        }

        ?>
        <tfoot>
        <tr>
            <th rowspan="1" colspan="1">#</th>
            <th rowspan="1" colspan="1">Tour</th>
            <th rowspan="1" colspan="1">First Name</th>
            <th rowspan="1" colspan="1">Last Name</th>
            <th rowspan="1" colspan="1">Email</th>
            <th rowspan="1" colspan="1"> Phone Number</th>
            <th rowspan="1" colspan="1"> Date Order</th>
            <th rowspan="1" colspan="1"> People</th>
            <th rowspan="1" colspan="1"> Instructor</th>

            <th style="text-align: center;" rowspan="1" colspan="1">Status</th>

            <th style="text-align: center;" rowspan="1" colspan="2"> Action</th>
        </tr>
        </tfoot>
    </table>
    <?php

}

//get id order
function getIdOrder()
{
    global $conn;


}

//edit order tour
function editOrderTour()
{
    global $conn;
    global $result1;

    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];
        $sql = "SELECT order_list.*,tour_list.tour_name FROM `order_list` 
INNER JOIN tour_list on tour_list.id= order_list.id
WHERE order_list.id= $id";

        $result1 = $conn->query($sql)->fetch();

    }


}

//Video slide
function videoSlide()
{
    global $conn;
    $video_sql = "SELECT * FROM `video_slide` ";
    $videos = $conn->query($video_sql)->fetch();

    return $videos;
}

//get all member
function getAllMember()
{
    global $conn;

    $member_sql = "SELECT * FROM `users` WHERE role=1 order  by id desc ";
    $members = $conn->query($member_sql)->fetchAll();
    return $members;
}

//create member
function createMember()
{
    global $conn;
    global $cdUpRefArray;
    if (isset($_POST['btn_save'])) {

        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $username = $_POST['username'];
        $user_sql = "SELECT * FROM `users` where username='$username' ";
        $users = $conn->query($user_sql)->fetch();
        if ($users['username'] == $username) {
            echo "<script>alert('Username exists')</script>";
            exit;
        } else {
            $username = $_POST['username'];
        }
        $password = $_POST['password'];
        $md5_password = md5($password);
        $phone_number = $_POST['phone_number'];
        $email = $_POST['email'];
        $status = $_POST['status'];
        $role = $_POST['role'];
        $sql = "INSERT INTO 
`users`(`id`, `first_name`, `last_name`, `username`, `password`, `email`, `phone_number`, `role`, `status`)
 VALUES (NULL ,'$first_name','$last_name','$username','$md5_password','$email','$phone_number',$role , $status)";
        $result = $conn->exec($sql);
        header("Location:" . globalUrl($cdUpRefArray) . "home/user/member/");
    }
}

//getIdMeber
function geIdMember()
{
    global $conn;

    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];

        $member_sql = "SELECT * FROM `users` WHERE id=$id";

        $members = $conn->query($member_sql)->fetch();
        return $members;
    } else {
        echo "Can't not find id memeber";
    }
}

//edit member
function editMember()
{
    global $conn;
    global $result;
    global $cdUpRefArray;
    $result = geIdMember();
    $id = $result['id'];
    if (isset($_POST['btn_save'])) {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];

        $status = $_POST['status'];
        $role = $_POST['role'];
        $sql = "UPDATE `users` SET
`first_name`='$first_name',`last_name`='$last_name',
 `role`=$role,`status`=$status
  WHERE id= $id";

        $members = $conn->exec($sql);
        header("Location:" . globalUrl($cdUpRefArray) . "home/user/member/");

    }

}

//deleteMember
function deleteMember()
{
    global $conn;
    global $cdUpRefArray;


    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];

        $sql = "DELETE FROM `users` WHERE id=$id";

        $result = $conn->prepare($sql);
        if ($result->execute()) {
            header("Location:" . globalUrl($cdUpRefArray) . "home/user/member/");
        } else {
            echo "<script>alert('Delete False')</script>";
            exit;
        }
    }
}

function getUser()
{
    global $conn;

    $username = $_SESSION['username'];
    $user_sql = " SELECT * FROM users where  username='$username'";
    $users = $conn->query($user_sql)->fetch();
    return $users;
}

//delete comment
function deleteComment()
{
    global $conn;
    global $cdUpRefArray;
    if (isset($_GET['cmt'])) {
        $id = $_GET['cmt'];
        $id_tour = $_GET['id'];
        $sql = "DELETE FROM `cmt` WHERE id=$id";

        $kq = $conn->prepare($sql);
        if ($kq->execute()) {

            echo "<script>alert('Delete Successful')</script>";
            header("Location:" . globalUrl($cdUpRefArray) . "home/comment/detail/?id=$id_tour");
        } else {
            echo "<script>alert('Delete false')</script>";

        }
    }
}


//get all settings
function getAllSetting()
{
    global $conn;
    try {
        $sql = "SELECT * FROM `settings` where  id= 1";
        $settings = $conn->query($sql)->fetch();

        return $settings;
    } catch (PDOException $e) {
        return $e->getMessage();
    }
}

?>






