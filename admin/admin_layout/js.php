<!-- jQuery 3 -->
<?php global $cdUpRefArray ?>
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/dist/js/demo.js"></script>
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/bower_components/ckeditor/ckeditor.js"></script>
<script src="<?php echo globalUrl($cdUpRefArray) ?>lib/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo globalUrl($cdUpRefArray) ?>js/ckeditor.js"></script>
<script src="<?php echo globalUrl($cdUpRefArray) ?>js/validate_image.js"></script>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');

        $('.textarea').wysihtml5()
    })
</script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function () {
        readURL(this);
    })
    ;</script>
<script >
    $(document).ready(function () {
        load_data();

        function load_data(query) {
            $.ajax({
                url:( "admin/controler/control.php"),
                method: "post",
                data: {query: query},
                success: function (data) {
                    $('#result').html(data);
                }
            });
        }

        $('#search_btn').click(function () {
            var search = $("#search_text").val();
            if (search != '') {
                load_data(search);
            } else {
                load_data();
            }
        });
    });

</script>

