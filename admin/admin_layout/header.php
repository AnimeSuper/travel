<?php
ob_start();
if (!isset($_SESSION['username'])) {
    header("Location:" . globalUrl($cdUpRefArray) . "index.php");
}
$users = getUser();
?>
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo globalUrl($cdUpRefArray) . "home/" ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>T</b>V</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Tour-</b>Travel</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="<?php echo globalUrl($cdUpRefArray) . "home/" ?>" class="dropdown-toggle"
                       data-toggle="dropdown">
                        <img src="<?php echo globalUrl($cdUpRefArray) ?>home/noimage.png" class="user-image"
                             alt="User Image">
                        <span class="hidden-xs"><?php echo $users['username']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo globalUrl($cdUpRefArray) ?>home/noimage.png" class="user-image"
                                 alt="User Image">
                            <p>
                                <?php echo $users['username']; ?>
                                <!--                                    <small>Member since Nov. 2012</small>-->
                            </p>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo globalUrl($cdUpRefArray) . "home/logout.php" ?>"
                                   class="btn btn-default btn-flat">Sign out</a>;
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>