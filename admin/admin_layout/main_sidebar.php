
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo globalUrl($cdUpRefArray)?>home/noimage.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $users['username'];?>
                </p>
                <!--                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">Tour-Travel</li>
            <li class=" active treeview menu-close">
                <a href="#">
                    <i class="fa fa-fw fa-archive"></i> <span>Tour</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li ><a href="<?php echo globalUrl($cdUpRefArray);?>home/tour/category/"><i class="fa fa-circle-o"></i>Category</a></li>
                    <li><a href="<?php echo globalUrl($cdUpRefArray);?>home/tour/detail_tour/"><i class="fa fa-circle-o"></i> Tour_list</a></li>
                    <li><a href="<?php echo globalUrl($cdUpRefArray);?>home/tour/order_tour/"><i class="fa fa-circle-o"></i> Order Tour</a></li>
                </ul>
            </li>
            <li class="  treeview menu-close">
                <a href="#">
                    <i class="fa fa-fw fa-archive"></i> <span>Users</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li ><a href="<?php echo globalUrl($cdUpRefArray);?>home/user/member/"><i class="fa fa-circle-o"></i>Member</a></li>
                    <li><a href="<?php echo globalUrl($cdUpRefArray);?>home/user/guilder/"><i class="fa fa-circle-o"></i> Instructor</a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo globalUrl($cdUpRefArray);?>home/video_slide/">
                    <i class="fa fa-fw fa-video-camera"></i> <span>Video-Slide</span>
                    <span class="pull-right-container">

            </span>
                </a>
            </li>
<!--            <li>-->
<!--                <a href="--><?php //echo globalUrl($cdUpRefArray);?><!--home/ads/">-->
<!--                    <i class="fa fa-fw fa-buysellads"></i></i> <span>Adsense</span>-->
<!--                    <span class="pull-right-container">-->
<!---->
<!--            </span>-->
<!--                </a>-->
<!--            </li>-->
            <li>
                <a href="<?php echo globalUrl($cdUpRefArray);?>home/comment/">
                    <i class="fa fa-fw fa-comment-o"></i> <span>Comment</span>
                    <span class="pull-right-container">

            </span>
                </a>
            </li>
            <li>
                <a href="<?php echo globalUrl($cdUpRefArray);?>home/setting/">
                    <i class="fa fa-fw fa-gears"></i> <span>Settings</span>
                    <span class="pull-right-container">

            </span>
                </a>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>
