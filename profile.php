<?php
ob_start();
include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php"); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <div class="form_popup from_login">
                    <div class="inner-form">
                        <div class="closeicon"></div>
                        <h3>Trang cá nhân</h3>
                        <?php
                        if (isset($_GET['user'])) {
                            $id_user = $_GET['user'];
                            $sql_profile = "SELECT * FROM `users` WHERE username='$id_user'";
                            $user_profiles = $conn->query($sql_profile)->fetch();
                        }
                        ?>
                        <form action="" method="post">
                            <p class="login-username">

                                <label for="user_login">Tài khoản</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;"  required type="text" name="username" disabled value="<?php echo md5($user_profiles['username']); ?>"
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Họ</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" required type="text" name="first_name" value="<?php echo $user_profiles['first_name'] ?>"
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Tên</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" required type="text" name="last_name" value="<?php echo $user_profiles['last_name'] ?>"
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Email</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" required type="text" name="email" value="<?php echo $user_profiles['email'] ?>"
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Số điện thoại</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" required type="text" name="phone_number" value="<?php echo $user_profiles['phone_number'] ?>"
                                       size="20">
                            </p>
                            <p class="login-submit">
                                <input style="width: 100%;
    padding: 12px 15px;
    text-align: center;
    background: #2a2a2a;
    color: #fff;
    border: none;
    text-transform: uppercase;
    max-width: 190px;" required type="submit" name="btn_save"
                                       class="button button-primary" value="Cập nhật">

                            </p>
                        </form>
                        <a href="lost_password.php" title="Đổi mật khẩu" class="lost-pass">Đổi mật khẩu tại đây!</a>
                    </div>
                    <?php
                    if (isset($_POST['btn_save']) && isset($_SESSION['user'])) {
                        $first_name = $_POST['first_name'];
                        $last_name = $_POST['last_name'];
                        $email = $_POST['email'];
                        $phone_number = $_POST['phone_number'];

                        $username1 = $user_profiles['username'];

                        $sql = "UPDATE `users` SET `first_name`='$first_name',`last_name`='$last_name',`email`='$email',`phone_number`='$phone_number' WHERE username='$username1'";
                        $profiles = $conn->exec($sql);
                        if ($profiles == 1) {
                            echo "<script>alert('Cập nhật thành công!')</script>";
                            header("Refresh:0");
                        }else{
                            echo "<script>alert('Thông tin không có gì để cập nhật!')</script>";
                        }

                    }
                    ?>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>