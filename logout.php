<?php
session_start();
if (isset($_SESSION['user'])) {
    session_unset($_SESSION['user']);

    header("Location:./index.php");
}

?>
