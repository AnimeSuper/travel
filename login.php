<?php
ob_start();
include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php"); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <div class="form_popup from_login">
                    <div class="inner-form">
                        <div class="closeicon"></div>
                        <h3>Đăng Nhập</h3>
                        <form action="" method="post">
                            <p class="login-username">
                                <label for="user_login">Tài Khoản</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="text" name="log" value=""
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Mật Khẩu</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="password" name="pwd" value=""
                                       size="20">
                            </p>

                            <p class="login-submit">
                                <input style="width: 100%;
    padding: 12px 15px;
    text-align: center;
    background: #2a2a2a;
    color: #fff;
    border: none;
    text-transform: uppercase;
    max-width: 190px;" type="submit" name="btn_login"
                                       class="button button-primary" value="Log In">

                            </p>
                        </form>
                        <a href="lost_password.php" title="Lost your password?" class="lost-pass">Lost your password?</a>
                    </div>
                    <?php
                    if (isset($_POST['btn_login'])) {
                        $username = $_POST['log'];

                        $password = $_POST['pwd'];
                        $user_sql = "SELECT * FROM users where username='$username'  ";
                        $users = $conn->query($user_sql)->fetch();
                        $password_md5 = md5($password);

                        if (!$username || !$password) {
                            echo "Vui lòng nhập tên và tài khoản ";
                            exit;

                        }
                        if ($username != $users['username']) {
                            echo "Tài khoản không tồn tại";
                            exit;

                        }
                        if ($password_md5 != $users['password']) {
                            echo "Mật khẩu sai ";
                            exit;

                        }
                        $_SESSION['user'] = $username;

                        header("Location:index.php");
                        ob_end_flush();
                    }
                    ?>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>