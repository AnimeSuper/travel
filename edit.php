<?php
ob_start();
include "admin/controler/global_url.php";
include("control.php");
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $edit_sql = "SELECT * FROM `order_list` WHERE id= $id";
    $result = $conn->query($edit_sql)->fetch();
}
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "./layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <div class="form_popup from_login">
                    <div class="inner-form">
                        <div class="closeicon"></div>

                        <form method="post">
                            <div class="form-group">
                                <label>Chọn ngày đi</label>
                                <input type="date" value="<?php echo $result['date_order'] ?>" name="date_book"
                                       class="form-control">

                            </div>
                            <div class="from-group">
                                <div class="total_price_arrow">
                                    <div class="st_adults_children">
                                        <label>Số người</label>
                                        <div class="input-number-ticket">
                                            <input type="number" name="number_ticket"
                                                   value="<?php echo $result['people'] ?>" min="1" max="10"
                                                   placeholder="Number ticket of Adults">
                                        </div>

                                    </div>


                                </div>
                            </div>


                            <button style="  margin-bottom: 300px"
                                    onclick="return confirm('Bạn có chắc chắn muốn cập nhật lại')" type="submit"
                                    name="btn_order" class="btn btn-primary">Cập nhật
                            </button>
                        </form>
                    </div>
                    <?php

                    if (isset($_POST['btn_order'])) {
                        $id_user = $users['id'];
                        $date_book = $_POST['date_book'];
                        $date = date_create($date_book);
                        $date_fm = date_format($date, "Y/m/d");
                        $people = $_POST['number_ticket'];
                        $book_sql = "UPDATE `order_list` SET `date_order`='$date_fm',`people`=$people WHERE id= $id";
                        $books = $conn->exec($book_sql);

                        header("Location:history_book.php?id=$id_user");

                    } ?>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>