<?php
ob_start();
include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php");
if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "SELECT order_list.*,tour_list.tour_name FROM `order_list` 
INNER JOIN tour_list on tour_list.id= order_list.id_tour WHERE order_list.id_users= $id ORDER by order_list.id DESC  ";

    $result1 = $conn->query($sql)->fetchAll();

}
?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <h2>Lịch sử đặt tour</h2>

                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tên tour</th>
                        <th scope="col">Họ</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Email</th>
                        <th scope="col">Số Điện Thoại</th>
                        <th scope="col">Ngày đi</th>
                        <th scope="col">Số Người</th>
                        <th scope="col">Trạng thái</th>
                        <th colspan="2" scope="col">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($result1

                                   as $key => $rows) { ?>
                        <tr>

                        <th scope="row"><?php echo $key ?></th>
                        <td><?php echo $rows['tour_name'] ?>
                            <br> ID:<?php echo $rows['id'] ?></td>
                        <td><?php echo $rows['first_name'] ?></td>
                        <td><?php echo $rows['last_name'] ?></td>
                        <td><?php echo $rows['email'] ?></td>
                        <td><?php echo $rows['phone_number'] ?></td>
                        <td><?php echo $rows['date_order'] ?></td>
                        <td>

                            <?php echo $rows['people'] ?>
                        </td>
                        <td style="text-align: center"><?php if ($rows['status'] == 3) {

                                echo '<span  class="badge badge-secondary">Đang chờ xét duyệt</span>';
                            } elseif ($rows['status'] == 0) {
                                echo '<span class="badge badge-danger">Từ chối</span>';
                            } elseif ($rows['status'] == 1) {
                                echo ' <span class="badge badge-primary">Chấp nhận</span>';
                            } elseif ($rows['status'] == 2) {
                                echo '<span class="badge badge-success">Hoàn thành</span>';
                            } ?></td>

                        <?php if ($rows['status'] == 3) { ?>
                            <td>
                                <a class="btn btn-primary" href="edit.php?id=<?php echo $rows['id'] ?>"
                                   role="button"> Sửa</a>
                            </td>
                            <td>
                                <a onclick="return confirm('Có chắc chắn muốn hủy tour!!')" class="btn btn-danger"
                                   href="deletetour.php?id=<?php echo $rows['id'] ?>&user=<?php echo $id ?>"
                                   role="button">Hủy Tour</a>
                            </td>
                            </tr>
                        <?php } else { ?>
                            <td>
                                <a disabled="" class="btn btn-primary"
                                   role="button"> Sửa</a>
                            </td>
                            <td>
                                <a disabled="" class="btn btn-danger"
                                   role="button">Hủy Tour</a>
                            </td>
                        <?php }
                    } ?>

                    </tbody>
                    <tfoot>
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tên tour</th>
                        <th scope="col">Họ</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Email</th>
                        <th scope="col">Số Điện Thoại</th>
                        <th scope="col">Ngày đi</th>
                        <th scope="col">Số Người</th>
                        <th scope="col">Trạng thái</th>
                        <th colspan="2" scope="col">Hành động</th>
                    </tr>
                    </thead>
                    </tfoot>
                </table>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>