<?php
ob_start();
include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php");

?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <div class="form_popup from_login">
                    <div class="inner-form">
                        <div class="closeicon"></div>
                        <h3>Đặt Tour</h3>    <a href="profile.php?user=<?php echo $_SESSION['user'] ?>">Thay đổi thông
                            tin tại đây</a>
                        <form method="post">
                            <div class="form-group">
                                <label>Họ</label>
                                <input type="text" readonly required name="first_name" class="form-control"
                                       value="<?php echo $users['first_name'] ?>">

                            </div>
                            <div class="form-group">
                                <label>Tên</label>
                                <input type="text" readonly name="last_name" class="form-control"
                                       value="<?php echo $users['last_name'] ?>">

                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" readonly name="email" class="form-control"
                                       value="<?php echo $users['email'] ?>">

                            </div>
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input type="text" readonly name="phone_number" class="form-control"
                                       value="<?php echo $users['phone_number'] ?>">

                            </div>
                            <div class="form-group">
                                <label>Chọn ngày đi</label>
                                <input type="date" required name="date_book" class="form-control">

                            </div>
                            <div class="from-group">
                                <div class="total_price_arrow">
                                    <div class="st_adults_children">
                                        <label>Số người</label>
                                        <div class="input-number-ticket">
                                            <input type="number" name="number_ticket" value="1" min="1" max="10"
                                                   placeholder="Number ticket of Adults">
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <label for="exampleFormControlTextarea1">Điều khoản đặt tour(Bắt buộc)</label>
                            <div style="border: 1px solid #ccc; padding: 20px;height: 300px;overflow: scroll"
                                 class="col-xs-12">
                                <?php echo $setting1['terms_of_service'] ?>
                            </div>
                            <div class="form-check">
                                <input required type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Tôi đã đọc và chấp nhận điều khoản
                                    trên</label>
                            </div>

                            <button onclick="return confirm('Bạn có chắc chắn muốn đặt tour')" type="submit"
                                    name="btn_order" class="btn btn-primary">Đặt tour
                            </button>
                        </form>
                    </div>
                    <?php

                    if (isset($_POST['btn_order'])) {
                        $first_name = $_POST['first_name'];
                        if (isset($_GET['tour']) && isset($_GET['instructor'])) {
                            $detail_tour = $_GET['tour'];
                            $id_instructor = $_GET['instructor'];
                        }
                        $id_tour = $detail_tour;
                        $id_instructor = $id_instructor;
                        $id_user = $users['id'];
                        $last_name = $_POST['last_name'];
                        $email = $_POST['email'];
                        $phone_number = $_POST['phone_number'];
                        $date_book = $_POST['date_book'];
                        $date = date_create($date_book);
                        $date_fm = date_format($date, "Y/m/d");
                        $id_user = $users['id'];
                        $people = $_POST['number_ticket'];
                        $book_sql = "INSERT INTO `order_list`
 VALUES (NULL ,$id_tour,$id_user,'$first_name','$last_name','$email','$phone_number','$date_fm',$people,$id_instructor,3)";

                        $books = $conn->exec($book_sql);
                        if ($books == 1) {
                            echo "<h5 style='background: #8def8d;'>Thông tin đang được xác nhận ! Vui lòng chờ</h5>";
                            echo "<a href='history_book.php?id=$id_user' >Lịch sử đặt tour</a>";
                        } else {
                            echo 'loi';
                        }
                    } ?>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>