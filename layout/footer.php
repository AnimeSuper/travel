<div class="wrapper-footer wrapper-footer-newsletter">
    <div class="main-top-footer">
        <div class="container">
            <div class="row">
                <aside class="col-sm-3 widget_text"><h3 class="widget-title">Liên Hệ</h3>
                    <div class="textwidget">
                        <div class="footer-info">
                            <p>Tour Du Lịch Hà Nội
                            </p>
                            <ul class="contact-info">
                                <li><i class="fa fa-map-marker fa-fw"></i><?php echo $setting1['address']?></li>
                                <li><i class="fa fa-phone fa-fw"></i> <?php echo $setting1['phone_number']?></li>
                                <li>
                                    <i class="fa fa-envelope fa-fw"></i><a href="mailto:<?php echo $setting1['email']?>"> <?php echo $setting1['email']?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </aside>

                <aside class="col-sm-3 widget_text"><h3 class="widget-title">MENU CỦA CHÚNG TÔI</h3>
                    <div class="textwidget">
                        <ul class="menu list-arrow">
                            <li><a href="index.php">Trang Chủ</a></li>
                            <li><a href="tours.php">Tours</a></li>
                            <li><a href="alert_update.php">Tin Tức</a></li>
                            <li><a href="alert_update.php">Liên Hệ</a></li>

                        </ul>
                    </div>
                </aside>
                <aside class="col-sm-6 widget_text"><h3 class="widget-title">Bản Đồ</h3>
                    <div class="textwidget">
                        <?php echo $setting1['map']?>
                    </div>
                </aside>
            </div>
        </div>
    </div>

</div>