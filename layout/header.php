<?php

ob_start(); ?>
    <header id="masthead" class="site-header sticky_header affix-top">
        <div class="header_top_bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <aside id="text-15" class="widget_text">
                            <div class="textwidget">
                                <ul class="top_bar_info clearfix">
                                    <li><i class="fa fa-clock-o"></i> <?php echo $setting1['work_time'] ?></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="col-sm-8 topbar-right">
                        <aside id="text-7" class="widget widget_text">
                            <div class="textwidget">
                                <ul class="top_bar_info clearfix">
                                    <li><i class="fa fa-phone"></i> <?php echo $setting1['phone_number'] ?></li>
                                    <li class="hidden-info">
                                        <i class="fa fa-map-marker"></i> <?php echo $setting1['address'] ?>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                        <aside id="travel_login_register_from-2" class="widget widget_login_form">

                            <?php if (isset($_SESSION['user'])) { ?>
                                <a href="profile.php?user=<?php echo $_SESSION['user'] ?>"> <?php echo $_SESSION['user'] ?></a>|
                                <a href="history_book.php?id=<?php echo $users['id']?>"> Lịch sử đặt tour</a>|
                                <a href="./logout.php">Đăng Xuất</a>
                            <?php } else { ?>
                                <a href="login.php"><i class="fa fa-user"></i> <span>Đăng Nhập</span></a> |
                                <a href="registered.php"><span>Đăng Ký</span></a>
                            <?php } ?>

                            <div class="background-overlay"></div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <div class="navigation-menu">
            <div class="container">
                <div class="menu-mobile-effect navbar-toggle button-collapse" data-activates="mobile-demo">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </div>
                <div class="width-logo sm-logo">
                    <a href="index.php" title="Travel" rel="home">
                        <img src="images/logo/<?php echo $setting1['logo'] ?>" alt="Logo" width="474" height="130"
                             class="logo_transparent_static">
                        <img src="images/logo/<?php echo $setting1['logo'] ?>" alt="Sticky logo" width="474"
                             height="130"
                             class="logo_sticky">
                    </a>
                </div>
                <nav class="width-navigation">
                    <ul class="nav navbar-nav menu-main-menu side-nav" id="mobile-demo" style="left: -250px;">
                        <li>
                            <a href="index.php">Trang Chủ</a>
                        </li>
                        <li><a href="alert_update.php">Giới thiệu</a></li>
                        <li class="menu-item-has-children">
                            <a href="tours.php">Tours</a>
                            <ul class="sub-menu">
                                <?php foreach ($category as $rows) { ?>
                                    <li>
                                        <a href="tours.php?id=<?php echo $rows['id'] ?>"><?php echo $rows['category'] ?></a>
                                    </li>

                                <?php } ?>

                                </li>
                            </ul>
                        </li>


                        <li><a href="alert_update.php">Tin Tức</a></li>


                        <li><a href="alert_update.php">Liên Hệ</a></li>
                        <li class="menu-right">
                            <ul>
                                <li id="travel_social_widget-2" class="widget travel_search">
                                    <div class="search-toggler-unit">
                                        <div class="search-toggler">
                                            <i class="fa fa-search"></i>
                                        </div>
                                    </div>
                                    <div class="search-menu search-overlay search-hidden">
                                        <div class="closeicon"></div>
                                        <form role="search" method="get" class="search-form" action="">
                                            <input type="search" class="search-field" placeholder="Search ..." value=""
                                                   name="search" title="Search for:">
                                            <input type="submit" name="btn_search" class="search-submit font-awesome"
                                                   value="">
                                        </form>

                                        <div class="background-overlay"></div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
<?php

ob_end_flush(); ?>