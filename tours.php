<?php include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php"); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <div class="row">
                    <div class="site-main col-sm-9 alignright">
                        <ul class="tours products wrapper-tours-slider">
                            <?php
                            $tour_sql = "SELECT * FROM `tour_list` WHERE status=1";

                            if (isset($_GET['id']) && !empty($_GET['id'])) {

                                $where = " and id_category=" . $_GET["id"] . "";
                                $tour_sql2 = $tour_sql . $where;

                            } else if (isset($_GET['btn_search']) && $_GET['btn_search'] != '') {
                                $search = $_GET['search'];
                                $tour_sql2 = "SELECT * FROM `tour_list`   WHERE (tour_name like '%$search%')or(location like '%$search%') ";

                            } else {
                                $tour_sql2 = $tour_sql;
                            }
                            $tour = $conn->query($tour_sql2)->fetchAll();

                            foreach ($tour as $rows) { ?>
                                <li class="item-tour col-md-4 col-sm-6 product">
                                    <div class="item_border item-product">
                                        <div class="post_images">
                                            <a href="single-tour.php?id=<?php echo $rows['id'] ?>">
                                                <span class="price">$<?php echo $rows['tour_cost'] ?></span>
                                                <img width="430" height="305"
                                                     src="images/tour/detail/<?php echo $rows['images'] ?>"
                                                >
                                            </a>

                                        </div>
                                        <div class="wrapper_content">
                                            <div class="post_title"><h4>
                                                    <a href="single-tour.php?id=<?php echo $rows['id'] ?>"
                                                       rel="bookmark"><?php echo $rows['tour_name'] ?></a>
                                                </h4></div>
                                            <span class="post_date"><?php echo $rows['duration'] ?></span>
                                            <div class="description">
                                                <p><?php echo $rows['short_desc'] ?></p>
                                            </div>
                                        </div>
                                        <div class="read_more">
                                            <div class="item_rating">
                                                <?php if ($rows['voting'] == 0) { ?>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>

                                                <?php } else if ($rows['voting'] == 1) { ?>
                                                    <i class="fa fa-star"></i>

                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <?php
                                                } else if ($rows['voting'] == 2) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                <?php } else if ($rows['voting'] == 3) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                <?php } else if ($rows['voting'] == 4) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                <?php } else if ($rows['voting'] == 5) { ?>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>

                                                <?php } ?>
                                            </div>
                                            <a rel="nofollow" href="single-tour.php?id=<?php echo $rows['id'] ?>"
                                               class="button product_type_tour_phys add_to_cart_button">Read more</a>
                                        </div>
                                    </div>
                                </li>

                            <?php } ?>
                        </ul>

                    </div>
                    <div class="widget-area align-left col-sm-3">
                        <div class="search_tour">
                            <div class="form-block block-after-indent">
                                <h3 class="form-block_title">Tìm Kiếm Tour</h3>
                                <div class="form-block__description">
                                    Tìm tour du lịch trong mơ của bạn ngày hôm nay!
                                </div>
                                <form role="search" method="get" action="">
                                    <input type="search" class="search-field" placeholder="Search ..." value=""
                                           name="search" title="Search for:">
                                    <input type="submit" name="btn_search" value="Tìm Kiếm">
                                </form>
                            </div>
                        </div>
                        <aside class="widget widget_travel_tour">
                            <div class="wrapper-special-tours">
                                <h4 style="text-align: center">Địa điểm nổi bật</h4>
                                <?php
                                $sql_tour = $tour_special_sql . " limit 5";
                                $tour_specials = $conn->query($sql_tour)->fetchAll();
                                foreach ($tour_specials as $special_row) { ?>
                                    <div class="inner-special-tours">
                                        <a href="single-tour.php?id=<?php echo $special_row['id'] ?>">
                                            <img width="430" height="305"
                                                 src="images/tour/detail/<?php echo $special_row['images'] ?>"
                                                 title="Hồ Tây"></a>
                                        <div class="item_rating">
                                            <?php if ($special_row['voting'] == 0) { ?>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>

                                            <?php } else if ($special_row['voting'] == 1) { ?>
                                                <i class="fa fa-star"></i>

                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <?php
                                            } else if ($special_row['voting'] == 2) { ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            <?php } else if ($special_row['voting'] == 3) { ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            <?php } else if ($special_row['voting'] == 4) { ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                            <?php } else if ($special_row['voting'] == 5) { ?>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>

                                            <?php } ?>
                                        </div>
                                        <div class="post_title"><h3>
                                                <a href="single-tour.php?id=<?php echo $special_row['id'] ?>"
                                                   rel="bookmark"><?php echo $special_row['tour_name'] ?></a>
                                            </h3></div>
                                        <div class="item_price">
                                            <span class="price">$<?php echo $special_row['tour_cost'] ?></span>

                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>