<?php
ob_start();
include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php");
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $detail_tour_sql = "SELECT tour_list.*,category.category, tour_instructor.fullname FROM `tour_list`
 INNER JOIN category on category.id= tour_list.id_category 
INNER JOIN tour_instructor on tour_instructor.id= tour_list.id_instructor
WHERE tour_list.id=$id";

    $detail_tour = $conn->query($detail_tour_sql)->fetch();
}

$album_sql = "SELECT * FROM `album_list` WHERE id_tour=$id";
$albums = $conn->query($album_sql)->fetchAll();

?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#" xmlns="http://www.w3.org/1999/html">

<!-- Mirrored from html.physcode.com/travel/single-tour.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:17 GMT -->
<head>
    <title>Single-Tour</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="single-product travel_tour-page travel_tour">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main" style="background-image:url(images/banner/top-heading.jpg);">

        </div>
        <section class="content-area single-woo-tour">
            <div class="container">
                <div class="tb_single_tour product">
                    <div class="top_content_single row">
                        <div class="images images_single_left">
                            <div class="title-single">
                                <div class="title">
                                    <h1><?php echo $detail_tour['tour_name'] ?></h1>
                                </div>
                                <div class="tour_code">
                                    <strong>Mã: </strong><?php echo $detail_tour['id'] ?>
                                </div>
                            </div>
                            <div class="tour_after_title">
                                <div class="meta_date">
                                    <span><?php echo $detail_tour['duration'] ?></span>
                                </div>
                                <div class="meta_values">
                                    <span>Thể Loại:</span>
                                    <div class="value">
                                        <a href="tours.php" rel="tag">
                                            <?php echo $detail_tour['category'] ?></a>

                                    </div>
                                </div>
                                <div class="meta_values">
                                    <span>Hướng dẫn viên:</span>
                                    <div class="value">
                                        <?php echo $detail_tour['fullname'] ?>

                                    </div>
                                </div>
                            </div>
                            <div id="slider" class="flexslider">
                                <ul class="slides">
                                    <?php foreach ($albums as $album_rows) { ?>
                                        <li>
                                            <a href="" class="swipebox" title="">
                                                <img width="950" height="700"
                                                     src="images/tour/detail/album/<?php echo $album_rows['images'] ?>"
                                                     class="attachment-shop_single size-shop_single wp-post-image"
                                                     alt="" title=""
                                                     draggable="false"></a>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div>
                            <div id="carousel" class="flexslider thumbnail_product">
                                <ul class="slides">
                                    <?php foreach ($albums as $album_rows) { ?>
                                        <li>
                                            <img width="150" height="100"
                                                 src="images/tour/detail/album/<?php echo $album_rows['images'] ?>"
                                                 class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image"
                                                 alt=""
                                                 title="" draggable="false">
                                        </li>

                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <div class="single-tour-tabs wc-tabs-wrapper">
                                <ul class="tabs wc-tabs" role="tablist">
                                    <li class="description_tab active" role="presentation">
                                        <a href="#tab-description" role="tab" data-toggle="tab">Mô Tả</a>
                                    </li>


                                    <li class="reviews_tab" role="presentation">
                                        <a href="#tab-reviews" role="tab" data-toggle="tab">Bình Luận </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel"
                                         class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--description panel entry-content wc-tab active"
                                         id="tab-description">
                                        <h2>   <?php echo $detail_tour['tour_name'] ?></h2>
                                        <?php echo $detail_tour['describe'] ?>
                                        <table class="tours-tabs_table">
                                            <tbody>
                                            <tr>
                                                <td><strong>VỊ TRÍ</strong></td>
                                                <td>  <?php echo $detail_tour['location'] ?></td>
                                            </tr>


                                            </tbody>
                                        </table>

                                    </div>

                                    <div role="tabpanel"
                                         class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--location_tab panel entry-content wc-tab"
                                         id="tab-location_tab">
                                        <div class="wrapper-gmap">
                                            <div id="googleMapCanvas" class="google-map" data-lat="50.893577"
                                                 data-long="-1.393483"
                                                 data-address="European Way, Southampton, United Kingdom"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel"
                                         class="tab-pane single-tour-tabs-panel single-tour-tabs-panel--reviews panel entry-content wc-tab"
                                         id="tab-reviews">
                                        <div id="reviews" class="travel_tour-Reviews">
                                            <div id="comments">

                                                <ol class="commentlist">
                                                    <li itemscope="" itemtype="http://schema.org/Review"
                                                        class="comment byuser comment-author-physcode bypostauthor even thread-even depth-1"
                                                        id="li-comment-62">
                                                        <div id="comment-62" class="comment_container">
                                                            <?php $comment_sql = "SELECT cmt.*,users.username FROM `cmt`INNER JOIN users on users.id = cmt.id_user WHERE cmt.id_tour=$id";

                                                            $comment = $conn->query($comment_sql)->fetchAll();

                                                            foreach ($comment as $comment_rows) {
//                                                            ?>
                                                                <div style="margin: 0" class="comment-text">

                                                                    <p class="meta">
                                                                        <strong><?php echo $comment_rows['username'] ?></strong>
                                                                        –
                                                                        <time>
                                                                            <?php echo $comment_rows['time_post'] ?>
                                                                        </time>
                                                                        :
                                                                    </p>
                                                                    <div class="description">
                                                                        <p><?php echo $comment_rows['comment'] ?></p>
                                                                    </div>

                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </li>
                                                </ol>
                                            </div>
                                            <div id="review_form_wrapper">
                                                <div id="review_form">
                                                    <div id="respond" class="comment-respond">
                                                        <h3 id="reply-title" class="comment-reply-title">Thêm bình
                                                            luận</h3>
                                                        <?php if (isset($_SESSION['user'])) { ?>
                                                            <form method="post" id="commentform" class="comment-form"
                                                            >
                                                                <p class="comment-form-comment">
                                                                    <label for="comment">Bình luận của bạn
                                                                        <span class="required">*</span></label>
                                                                    <textarea
                                                                            id="comment" name="comment" cols="45"
                                                                            rows="8"
                                                                            required></textarea>
                                                                </p>
                                                                <p class="form-submit">
                                                                    <input name="btn_comment" type="submit"
                                                                           class="submit" value="Submit">
                                                                </p></form>
                                                        <?php } else { ?>
                                                            <form method="post" id="commentform" class="comment-form"
                                                                  novalidate="">
                                                                <p class="comment-form-comment">
                                                                    <label for="comment">Bình luận của bạn
                                                                        <span class="required">*</span></label>
                                                                    <textarea
                                                                            disabled id="comment"
                                                                            name="comment"
                                                                            cols="45"
                                                                            rows="8"
                                                                            required>Vui lòng đăng nhập</textarea>
                                                                </p>
                                                                <p class="form-submit">
                                                                    <a href="login.php">Đăng nhập tại đây</a>
                                                                </p></form>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if (isset($_POST['btn_comment'])) {
                                                $id_tour = $id;
                                                $id_user = $users['id'];
                                                $comment = $_POST['comment'];
                                                $date = date("Y/m/d");
                                                $sql_comment = "INSERT INTO `cmt`(`id`, `id_tour`, `comment`, `id_user`, `time_post`, `status`)
 VALUES (NULL ,$id_tour,'$comment',$id_user,'$date',1)";

                                                $comments = $conn->exec($sql_comment);

                                                header("Refresh:0");
                                                ob_end_flush();
                                            } ?>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="related tours">
                                <h2>Tour Liên Quan</h2>
                                <ul class="tours products wrapper-tours-slider">
                                    <?php
                                    $id_category = $detail_tour['id_category'];

                                    $sample_tour_sql = "SELECT * FROM `tour_list` WHERE id_category=$id_category not in (id=$id)";


                                    $sample_tour = $conn->query($sample_tour_sql)->fetchAll();
                                    foreach ($sample_tour as $row) {
                                        ?>
                                        <li class="item-tour col-md-4 col-sm-6 product">
                                            <div class="item_border item-product">
                                                <div class="post_images">
                                                    <a href="single-tour.php?id=<?php echo $row['id']?>">
                                                        <span class="price">$93.00</span>
                                                        <img width="430" height="305"
                                                             src="images/tour/detail/<?php echo $row['images'] ?> "
                                                             alt="Discover Brazil" title="Discover Brazil">
                                                    </a>

                                                </div>
                                                <div  class="wrapper_content">
                                                    <div class="post_title"><h4>
                                                            <a href="single-tour.php?id=<?php echo $row['id']?>"
                                                               rel="bookmark"><?php echo $row['tour_name'] ?></a>
                                                        </h4></div>
                                                    <span class="post_date"><?php echo $row['duration'] ?></span>
                                                    <div class="description">
                                                        <p><?php echo $row['short_desc'] ?></p>
                                                    </div>
                                                </div>
                                                <div class="read_more">
                                                    <div class="item_rating">
                                                        <?php if ($row['voting'] == 0) { ?>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>

                                                        <?php } else if ($row['voting'] == 1) { ?>
                                                            <i class="fa fa-star"></i>

                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <?php
                                                        } else if ($row['voting'] == 2) { ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                        <?php } else if ($row['voting'] == 3) { ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                        <?php } else if ($row['voting'] == 4) { ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star-o"></i>
                                                        <?php } else if ($row['voting'] == 5) { ?>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>

                                                        <?php } ?>
                                                    </div>
                                                    <a rel="nofollow" href="single-tour.php?id=<?php echo $row['id'] ?>"
                                                       class="button product_type_tour_phys add_to_cart_button">Read
                                                        more</a>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="summary entry-summary description_single">
                            <div class="affix-sidebar">
                                <div class="entry-content-tour">

                                    <div class="clear"></div>
                                    <div class="booking">
                                        <div class="">
                                            <div class="form-block__title">
                                                <h4>Đặt tour</h4>
                                            </div>
                                            <?php if (isset($_SESSION['user'])) { ?>
                                                <div class="col-sm-12 text-center ">
                                                    <a href="order_tour.php?tour=<?php echo $detail_tour['id']?>&instructor=<?php echo $detail_tour['id_instructor']?>" class="icon-btn" title="">
                                                        </i>Đặt tour tại đây
                                                    </a>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-sm-12 text-center padding-top-6x">
                                                    <span>Vui lòng đăng nhập để đặt tour</span>
                                                    <a href="login.php" class="icon-btn" title="">
                                                        </i>Đăng nhập tại đây
                                                    </a>
                                                </div>
                                            <?php } ?>
                                            <?php

                                            if (isset($_POST['btn_order'])) {
                                                $first_name = $_POST['first_name'];
                                                $id_tour = $detail_tour['id'];
                                                $id_instructor = $detail_tour['id_instructor'];
                                                $id_user = $users['id'];
                                                $last_name = $_POST['last_name'];
                                                $email = $_POST['email'];
                                                $phone_number = $_POST['phone_number'];
                                                $date_book = $_POST['date_book'];
                                                $date = date_create($date_book);
                                                $date_fm = date_format($date, "Y/m/d");

                                                $people = $_POST['number_ticket'];
                                                $book_sql = "INSERT INTO `order_list`
 VALUES (NULL ,$id_tour,$id_user,'$first_name','$last_name','$email','$phone_number','$date_fm',$people,$id_instructor,3)";

                                                $books = $conn->exec($book_sql);
                                                if ($books == 1) {
                                                    echo "<h5 style='background: #8def8d;'>Thông tin đang được xác nhận ! Vui lòng chờ</h5>";
                                                } else {
                                                    echo 'loi';
                                                }
                                            } ?>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
<script type='text/javascript'
        src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCaQjbVDR1vRh2iS_V3jLBXRrkQxmoxycQ'></script>
<script type='text/javascript' src='assets/js/gmap.js'></script>

</body>

<!-- Mirrored from html.physcode.com/travel/single-tour.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>