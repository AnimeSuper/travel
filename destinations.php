<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/destinations.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Destination</title>
    <?php include "./layout/head.php"?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php"?>
	<div class="site wrapper-content">
		<div class="top_site_main" style="color: rgb(255, 255, 255); background-color: rgb(0, 0, 0); background-image: url('./images/home/brazil.jpg')">
			<div class="banner-wrapper-destination container article_heading text-center">
				<h1 class="heading_primary">Tourist Brazil</h1>
				<div class="desc"><p>Discover the Brazil with our special tours</p>
				</div>
				<div class="breadcrumbs-wrapper">
					<ul class="phys-breadcrumb">
						<li><a href="index.php" class="home">Home</a></li>
						<li><a href="tours.php" title="Tours">Tours</a></li>
						<li>Brazil</li>
					</ul>
				</div>
			</div>
		</div>
		<section class="content-area">
			<div class="container">
				<div class="row">
					<div class="site-main col-sm-12 full-width">
						<ul class="tours products wrapper-tours-slider">
							<li class="item-tour col-md-3 col-sm-6 product">
								<div class="item_border item-product">
									<div class="post_images">
										<a href="single-tour.php">
											<span class="price">$93.00</span>
											<img width="430" height="305" src="images/tour/430x305/tour-1.jpg" alt="Discover Brazil" title="Discover Brazil">
										</a>

									</div>
									<div class="wrapper_content">
										<div class="post_title"><h4>
											<a href="single-tour.php" rel="bookmark">Discover Brazil</a>
										</h4></div>
										<span class="post_date">5 DAYS 4 NIGHTS</span>
										<div class="description">
											<p>Aliquam lacus nisl, viverra convallis sit amet&nbsp;penatibus nunc&nbsp;luctus</p>
										</div>
									</div>
									<div class="read_more">
										<div class="item_rating">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
										</div>
										<a rel="nofollow" href="single-tour.php" class="button product_type_tour_phys add_to_cart_button">Read more</a>
									</div>
								</div>
							</li>
							<li class="item-tour col-md-3 col-sm-6 product">
								<div class="item_border item-product">
									<div class="post_images">
										<a href="single-tour.php">
											<span class="price"><del>$87.00</del>
												<ins>$82.00</ins>
											</span>
											<span class="onsale">Sale!</span>
											<img width="430" height="305" src="images/tour/430x305/tour-2.jpg" alt="Discover Brazil" title="Discover Brazil">
										</a>

									</div>
									<div class="wrapper_content">
										<div class="post_title"><h4>
											<a href="single-tour.php" rel="bookmark">Kiwiana Panorama</a>
										</h4></div>
										<span class="post_date">5 DAYS 4 NIGHTS</span>
										<div class="description">
											<p>Aliquam lacus nisl, viverra convallis sit amet&nbsp;penatibus nunc&nbsp;luctus</p>
										</div>
									</div>
									<div class="read_more">
										<div class="item_rating">
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
										</div>
										<a rel="nofollow" href="single-tour.php" class="button product_type_tour_phys add_to_cart_button">Read more</a>
									</div>
								</div>
							</li>

						</ul>
						<div class="navigation paging-navigation" role="navigation">
							<ul class="page-numbers">
								<li><span class="page-numbers current">1</span></li>
								<li><a class="page-numbers" href="#">2</a></li>
								<li><a class="next page-numbers" href="#"><i class="fa fa-long-arrow-right"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
    <?php include "./layout/footer.php"?>

</div>
<!--end coppyright-->
<script type='text/javascript' src='assets/js/jquery.min.js'></script>
<script type='text/javascript' src='assets/js/bootstrap.min.js'></script>
<script type='text/javascript' src='assets/js/vendors.js'></script>
<script type='text/javascript' src='assets/js/isotope.pkgd.min.js'></script>
<script type='text/javascript' src='assets/js/jquery.swipebox.min.js'></script>
<script type='text/javascript' src='assets/js/theme.js'></script>
</body>

<!-- Mirrored from html.physcode.com/travel/destinations.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:37 GMT -->
</html>