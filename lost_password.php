<?php
ob_start();
include_once "admin/controler/global_url.php";
include(globalUrl($cdUpRefArray) . "control.php"); ?>
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
<head>
    <title>Tours</title>
    <?php include "./layout/head.php" ?>
</head>

<body class="archive travel_tour travel_tour-page">
<div class="wrapper-container">
    <?php include "layout/header.php" ?>
    <div class="site wrapper-content">
        <div class="top_site_main"
             style="background-image: url(&quot;images/banner/top-heading.jpg&quot;); padding-top: 126px;">

        </div>
        <section class="content-area">
            <div class="container">
                <div class="form_popup from_login">
                    <div class="inner-form">
                        <div class="closeicon"></div>
                        <?php if (isset($_SESSION['user'])) { ?>
                            <h3>Đổi Mật Khẩu</h3>
                        <?php }else{?>
                            <h3>Tìm Lại Mật Khẩu</h3>
                      <?php  } ?>
                        <form action="" method="post">
                            <p class="login-username">
                                <label for="user_login">Tên tài khoản</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="text" name="username" value=""
                                       size="20">
                            </p>
                            <p class="login-username">
                                <label for="user_login">Mật khẩu cũ</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="password" name="old_pwd" value=""
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Mật khẩu mới</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="password" name="pwd" value=""
                                       size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass">Nhập lại mật khẩu mới</label>
                                <input style="    width: 100%;
    border: 1px solid #ddd;
    padding: 10px 15px;
    font-weight: normal;" type="password" name="re_pwd" value=""
                                       size="20">
                            </p>
                            <p class="login-submit">
                                <input style="width: 100%;
    padding: 12px 15px;
    text-align: center;
    background: #2a2a2a;
    color: #fff;
    border: none;
    text-transform: uppercase;
    max-width: 190px;" type="submit" name="btn_save"
                                       class="button button-primary" value="Save">

                            </p>
                        </form>

                    </div>
                    <?php
                    if (isset($_POST['btn_save'])) {
                        $username = $_POST['username'];
                        $user_sql = "SELECT * FROM users where username='$username'  ";
                        $users = $conn->query($user_sql)->fetch();
                        if ($username != $users['username']) {
                            echo "Tài khoản không tồn tại";
                            exit;

                        }
                        $old_pwd = $_POST['old_pwd'];
                        $md5_old_pwd = md5($old_pwd);
                        if ($md5_old_pwd == $users['password']) {
                            $pwd = $_POST['pwd'];


                            if ($pwd == $_POST['re_pwd']) {
                                $re_pwd = $_POST['re_pwd'];
                                $md5_pwd = md5($re_pwd);
                                $update_pwd_sql = "UPDATE `users` SET`password`='$md5_pwd' WHERE username='$username'";
                                $update_new_pwd = $conn->exec($update_pwd_sql);
                                if ($update_new_pwd == 1) {
                                    echo "Đổi mật khẩu thành công";
                                } else {
                                    echo "Đổi mật khẩu thất bại";
                                }
                            } else {
                                echo "Mật khẩu nhập lại không đúng";
                                exit;
                            }

                        } else {
                            echo "Mật khẩu cũ không đúng";
                            exit;
                        }


                    }
                    ?>
                </div>
            </div>
        </section>
    </div>
    <?php include "./layout/footer.php" ?>

</div>
<!--end coppyright-->
<?php include "./layout/js/js.php" ?>
</body>

<!-- Mirrored from html.physcode.com/travel/tours-4-cols.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 09:59:21 GMT -->
</html>